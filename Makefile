.PHONY: resources styles migrate

resources:
	cp -r node_modules resources/public

styles:
	npm run css-build
	npm run css-build-sandbox

app: resources styles
	clojure -M:app

migrate:
	clj -X:migrate
