(ns migrate
  (:require
   [reitit.core :as r]
   [reitit.ring :as ring]

   [migratus.core :as migratus]
   [activity-tracker.db.migrations
    :refer [config]
    :rename {config mig-config}]))


(comment
  (migratus/up mig-config 20211120005552 20211120112002 20220124170921)

  )
