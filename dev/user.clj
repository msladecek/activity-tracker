(ns user
  (:require
   [reitit.core :as r]
   [reitit.ring :as ring]

   [migratus.core :as migratus]
   [activity-tracker.db.migrations
    :refer [config]
    :rename {config mig-config}]
   [clojure.pprint :refer [pprint]]
   [activity-tracker.components :as components]
   [activity-tracker.model :as model]
   [activity-tracker.app :as app]
   [activity-tracker.external.goodreads :as goodreads]
   [muuntaja.core :as m]))


(defonce app (atom nil))


(defn start-app []
  (reset! app (app/start! {:port 3000})))


(defn stop-app []
  (when-some [a @app]
    (.stop a)
    (reset! app nil)))


(defn restart-app []
  (stop-app)
  (start-app))


(comment
  (restart-app)

  (stop-app)


  (def db
    (next.jdbc/get-datasource (activity-tracker.db.utilities/parse-url (System/getenv "DATABASE_URL"))))

  (model/presets db)


  (-> app/app
      (reitit.ring/get-router)
      (reitit.core/compiled-routes))


  (def reviews
    (goodreads/get-reviews {:shelf "currently-reading"}))


  (def review (goodreads/get-review {:book_id 77711}))

  (pprint (first review))

  (goodreads/remap-review review)

  (pprint (mapv goodreads/remap-review reviews))


  (db.tag/tag-exists? db {:category "book_series",
                          :category_label "Series",
                          :value "Firefall"})


  (migratus/init mig-config)


  (def router
    (r/router [["/bye" {:middleware ["root"]}
                ["/" {:name :bye}]
                ["/moon" {:name :bye-moon :middleware ["not-root"]}]]
               ["/hello/world" {:name :hello
                                :middleware "abc"}]]))

  (-> (r/match-by-path router "/hello/world")
      :data)

  (-> (r/match-by-path router "/bye/moon")
      )

  (r/routes router)


  (clojure.walk/prewalk
   (fn [thing] (println (meta thing) thing) thing)
   (components/preset-card #:preset{:id 1 :label "m" :is_active false :latest_interval nil :tags []}))


  (->> {:hello :world}
       (m/encode "application/json")
       slurp)

  (def router
    (ring/router
     ["/hello" {:name :hello}]
     {:data {:handler (fn [req] {:status 200})}}))

  (tap> (r/match-by-name router :hello))

  )
