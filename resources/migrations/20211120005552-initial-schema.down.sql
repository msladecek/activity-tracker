drop sequence if EXISTS public.tags_id_seq;
--;;
drop table if EXISTS public.tag;
--;;
drop sequence if EXISTS public.presets_id_seq;
--;;
drop table IF NOT EXISTS public.preset;
--;;
drop sequence if EXISTS public.intervals_id_seq;
--;;
drop sequence if EXISTS public.clocked_interval;
--;;
drop INDEX IF EXISTS fki_fk__clocked_interval__preset_id;
--;;
drop sequence EXISTS public.goals_id_seq;
--;;
drop table EXISTS public.goal;
--;;
drop index if EXISTS fki_fk__goals__tag_id;
--;;
drop sequence if EXISTS public.interval_preset_id_seq;
--;;
drop table if EXISTS public.interval_preset;
--;;
drop index if EXISTS fki_fk__interval_preset__interval_id;
--;;
drop index if EXISTS fki_fk__interval_preset__preset_id;
--;;
drop sequence if EXISTS public.preset_tag_id_seq;
--;;
drop table if EXISTS public.preset_tag;
--;;
drop index if EXISTS fki_fk__preset_tag__preset_id;
--;;
drop index if EXISTS fki_fk__preset_tag__tag_id;
