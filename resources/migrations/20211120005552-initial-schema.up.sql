CREATE SEQUENCE IF NOT EXISTS public.tags_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
--;;
ALTER SEQUENCE public.tags_id_seq
    OWNER TO postgres;
--;;
CREATE TABLE IF NOT EXISTS public.tag
(
    id bigint NOT NULL DEFAULT nextval('tags_id_seq'::regclass),
    category text COLLATE pg_catalog."default" NOT NULL,
    category_label text COLLATE pg_catalog."default" NOT NULL,
    value text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tags_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;
--;;
alter sequence public.tags_id_seq
    OWNED BY tag.id;
--;;
ALTER TABLE IF EXISTS public.tag
    OWNER to postgres;
--;;
CREATE SEQUENCE IF NOT EXISTS public.presets_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
--;;
ALTER SEQUENCE public.presets_id_seq
    OWNER TO postgres;
--;;
CREATE TABLE IF NOT EXISTS public.preset
(
    id bigint NOT NULL DEFAULT nextval('presets_id_seq'::regclass),
    is_hidden boolean NOT NULL DEFAULT false,
    label text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default" NOT NULL DEFAULT 'generic'::text,
    layout text[] COLLATE pg_catalog."default",
    CONSTRAINT presets_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;
--;;
alter sequence public.presets_id_seq
    OWNED BY preset.id;
--;;
ALTER TABLE IF EXISTS public.preset
    OWNER to postgres;
--;;
CREATE SEQUENCE IF NOT EXISTS public.intervals_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
--;;
ALTER SEQUENCE public.intervals_id_seq
    OWNER TO postgres;
--;;
CREATE TABLE IF NOT EXISTS public.clocked_interval
(
    id bigint NOT NULL DEFAULT nextval('intervals_id_seq'::regclass),
    active_during tstzrange NOT NULL,
    preset_id bigint,
    is_deleted boolean NOT NULL DEFAULT false,
    CONSTRAINT intervals_pkey PRIMARY KEY (id),
    CONSTRAINT fk__clocked_interval__preset_id FOREIGN KEY (preset_id)
        REFERENCES public.preset (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE pg_default;
--;;
alter sequence public.intervals_id_seq
    OWNED BY clocked_interval.id;
--;;
ALTER TABLE IF EXISTS public.clocked_interval
    OWNER to postgres;
--;;
CREATE INDEX IF NOT EXISTS fki_fk__clocked_interval__preset_id
    ON public.clocked_interval USING btree
    (preset_id ASC NULLS LAST)
    TABLESPACE pg_default;
--;;
CREATE SEQUENCE IF NOT EXISTS public.goals_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
--;;
ALTER SEQUENCE public.goals_id_seq
    OWNER TO postgres;
--;;
CREATE TABLE IF NOT EXISTS public.goal
(
    id bigint NOT NULL DEFAULT nextval('goals_id_seq'::regclass),
    tag_id bigint NOT NULL,
    value_hours bigint NOT NULL DEFAULT 1,
    window_type text COLLATE pg_catalog."default" NOT NULL DEFAULT 'day'::text,
    CONSTRAINT goals_pkey PRIMARY KEY (id),
    CONSTRAINT fk__goals__tag_id FOREIGN KEY (tag_id)
        REFERENCES public.tag (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE pg_default;
--;;
alter sequence public.goals_id_seq
    OWNED BY goal.id;
--;;
ALTER TABLE IF EXISTS public.goal
    OWNER to postgres;
--;;
CREATE INDEX IF NOT EXISTS fki_fk__goals__tag_id
    ON public.goal USING btree
    (tag_id ASC NULLS LAST)
    TABLESPACE pg_default;
--;;
CREATE SEQUENCE IF NOT EXISTS public.interval_preset_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
--;;
CREATE TABLE IF NOT EXISTS public.interval_preset
(
    id bigint NOT NULL DEFAULT nextval('interval_preset_id_seq'::regclass),
    interval_id bigint NOT NULL,
    preset_id bigint NOT NULL,
    CONSTRAINT interval_preset_pkey PRIMARY KEY (id),
    CONSTRAINT fk__interval_preset__interval_id FOREIGN KEY (interval_id)
        REFERENCES public.clocked_interval (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk__interval_preset__preset_id FOREIGN KEY (preset_id)
        REFERENCES public.preset (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE pg_default;
--;;
ALTER SEQUENCE public.interval_preset_id_seq
    OWNED BY interval_preset.id;
--;;
ALTER SEQUENCE public.interval_preset_id_seq
    OWNER TO postgres;
--;;
ALTER TABLE IF EXISTS public.interval_preset
    OWNER to postgres;
--;;
CREATE INDEX IF NOT EXISTS fki_fk__interval_preset__interval_id
    ON public.interval_preset USING btree
    (interval_id ASC NULLS LAST)
    TABLESPACE pg_default;
--;;
CREATE INDEX IF NOT EXISTS fki_fk__interval_preset__preset_id
    ON public.interval_preset USING btree
    (preset_id ASC NULLS LAST)
    TABLESPACE pg_default;
--;;
CREATE SEQUENCE IF NOT EXISTS public.preset_tag_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
--;;
CREATE TABLE IF NOT EXISTS public.preset_tag
(
    id bigint NOT NULL DEFAULT nextval('preset_tag_id_seq'::regclass),
    tag_id bigint NOT NULL,
    preset_id bigint NOT NULL,
    CONSTRAINT preset_tag_pkey PRIMARY KEY (id),
    CONSTRAINT fk__preset_tag__preset_id FOREIGN KEY (preset_id)
        REFERENCES public.preset (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk__preset_tag__tag_id FOREIGN KEY (tag_id)
        REFERENCES public.tag (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE pg_default;
--;;
ALTER SEQUENCE public.preset_tag_id_seq
    OWNED BY preset_tag.id;
--;;
ALTER SEQUENCE public.preset_tag_id_seq
    OWNER TO postgres;
--;;
ALTER TABLE IF EXISTS public.preset_tag
    OWNER to postgres;
--;;
CREATE INDEX IF NOT EXISTS fki_fk__preset_tag__preset_id
    ON public.preset_tag USING btree
    (preset_id ASC NULLS LAST)
    TABLESPACE pg_default;
--;;
CREATE INDEX IF NOT EXISTS fki_fk__preset_tag__tag_id
    ON public.preset_tag USING btree
    (tag_id ASC NULLS LAST)
    TABLESPACE pg_default;
