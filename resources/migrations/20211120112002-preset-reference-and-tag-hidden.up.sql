ALTER TABLE IF EXISTS public.preset
    ADD COLUMN reference text COLLATE pg_catalog."default";
--;;
ALTER TABLE IF EXISTS public.tag
    ADD COLUMN is_hidden boolean NOT NULL DEFAULT false;
