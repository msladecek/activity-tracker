(ns activity-tracker.app
  (:require
   [activity-tracker.components :as components]
   [activity-tracker.db.utilities :as db.utilities]
   [activity-tracker.handlers :refer [data-handler]]
   [activity-tracker.model :as model]
   [activity-tracker.templates :refer [hiccup-formatter]]
   [activity-tracker.utilities :refer [->json]]
   [buddy.auth.backends.token :refer [jws-backend]]
   [buddy.auth.middleware :refer [authenticate-request]]
   [buddy.sign.jwt :as jwt]
   [clojure.java.io :as io]
   [clojure.walk :refer [prewalk]]
   [hiccup.core :refer [html] :rename {html hiccup->html}]
   [muuntaja.middleware :refer [wrap-format]]
   [next.jdbc :as jdbc]
   [reitit.coercion.malli :refer [coercion] :rename {coercion malli-coercion}]
   [reitit.coercion]
   [reitit.core :as r]
   [reitit.ring.coercion :refer [coerce-request-middleware coerce-response-middleware coerce-exceptions-middleware]]
   [reitit.ring.middleware.parameters :refer [parameters-middleware]]
   [reitit.ring]
   [ring.adapter.jetty :as jetty]
   [ring.middleware.cookies :refer [wrap-cookies]]
   [ring.util.mime-type :refer [ext-mime-type]]
   [ring.util.response :refer [file-response content-type]]
   [tick.core :as t]
   ))


(defn substitute-routes [router form]
  (letfn [(swap-obj [obj]
            (if (:route (meta obj))
              (let [[route-name path-params query-params] obj]
                (-> (r/match-by-name router route-name path-params)
                    (r/match->path query-params)))
              obj))]
    (prewalk swap-obj form)))

(defn static-resource-handler [req]
  (let [path (str "public/" (get-in req [:path-params :path] (:uri req)))
        resource (io/resource path)]
    (if resource
      (-> (.getPath resource)
          file-response
          (content-type (ext-mime-type path)))
      (throw (ex-info "static resource not found"
                      {:status 404 :title "Not Found"})))))

(defn assoc-routed-handler-name [handler]
  (fn [req]
    (handler (assoc req :handler-name (get-in req [:reitit.core/match :data :name])))))

(defn assoc-route-substitutor [handler]
  (fn [req]
    (handler (assoc req :route-substitutor (fn [form] (substitute-routes (:reitit.core/router req) form))))))

(defn load-auth-from-cookie [handler]
  (fn [req]
    (if-not (get-in req [:headers "authorization"])
      (if-let [cookie-token (get-in req [:cookies "_activity-tracker_token" :value])]
        (handler (assoc-in req [:headers "authorization"] (str "Token " cookie-token)))
        (handler req))
      (handler req))))

(defn authenticate [handler]
  (fn [request]
    (let [auth-secret (System/getenv "AUTH_SECRET")
          auth-backend (jws-backend {:secret auth-secret})
          authdata (authenticate-request request [auth-backend])]
      (if-not (and authdata
                   (t/> (t/>> (t/instant (* 1000 (:iat authdata))) (t/new-duration 1 :days))
                        (t/instant)))
        {:status 302
         :headers {"Location" (str "/login" "?location=" (jwt/sign {:location (:uri request)} auth-secret))}}
        (handler request)))))

(defn provide-db [handler datasource]
  (fn [request]
    (handler (assoc request :db datasource))))

(defn handler [req]
  (let [data (data-handler req)]
    (cond-> {:status 200
             :data {:content data
                    :meta {:handler-name (:handler-name req)
                           :request-method (:request-method req)}}}
      (nil? data) (assoc :status 204)
      (meta data) (into (meta data)))))

#_(defn exception-middleware [handler]
  (fn [req]
    (try
      (handler req)
      (catch Exception exc
        (if-let [data (ex-data exc)]
          (let [status (get data :status 500)]
            (cond-> {:status status :data (assoc data :error true)}
              (= status 405) (assoc-in [:headers "Allow"] (str/join "," (:allowed-methods data)))))
          {:status 500 :data "internal server error"})))))

(defn wrap-page-hiccup-data [handler]
  (fn [req]
    (let [response (handler req)]
      (update-in response [:data :hiccup] components/page req))))

(defn render-hiccup-data [handler]
  (fn [req]
    (let [response (handler req)]
      (if (:body response)
        response
        (let [route-substitutor (get req :route-substitutor identity)]
          (assoc response :body (-> (get-in response [:data :hiccup])
                                    route-substitutor
                                    hiccup->html
                                    str)))))))

(defn render-json-data [handler]
  (fn [req]
    (let [response (handler req)]
      (if (:body response)
        response
        (let [content (get-in response [:data :content])]
          (assoc response :body (->json content)))))))

(defn load-hiccup-template [handler]
  (fn [req]
    (let [response (handler req)
          response-data (:data response)]
      (->> (assoc response-data :parameters (:parameters req))
           hiccup-formatter
           (assoc-in response [:data :hiccup])))))

(defn ex-data-middleware [handler]
  (fn [req]
    (try
      (handler req)
      (catch Exception e
        (if-let [data (ex-data e)]
          {:status (:status data)
           :body (->json e)
           :headers {"content-type" "application/json"}}
          (throw e))))))

(def ^:private malli-positive-int
  [:and [:> 0] :int])

(defn preload-preset-with-tags [handler]
  (fn [{:keys [db parameters] :as req}]
    (let [preset (model/preset-with-tags db (get-in parameters [:path :preset-id]))]
      (handler (assoc-in req [:preloads :preset] preset)))))

(defn preload-interval [handler]
  (fn [{:keys [db parameters] :as req}]
    (let [interval (model/interval-with-preset-data db (get-in parameters [:path :interval-id]))]
      (handler (assoc-in req [:preloads :interval] interval)))))

(defn preload-goal-with-tags-and-intervals [handler]
  (fn [{:keys [db parameters] :as req}]
    (let [goal (model/tagged-and-clocked-goal db (get-in parameters [:path :goal-id]))]
      (handler (assoc-in req [:preloads :goal] goal)))))

(defn preload-tag [handler]
  (fn [{:keys [db parameters] :as req}]
    (let [tag (model/tag db (get-in parameters [:path :tag-id]))]
      (handler (assoc-in req [:preloads :tag] tag)))))

;; TODO replace these `parse-*-form-params` helpers with some custom malli logic
(defn parse-preset-form-params [handler]
  (fn [req]
    (let [params (:form-params req)
          tag-ids (->> (for [param (keys params)
                             :let [[_ tag-id-str] (re-find #"^tag-(\d+)$" param)]
                             :when tag-id-str]
                         (Integer/parseInt tag-id-str))
                       (into #{}))]
      (-> req
          (assoc-in [:parameters :form :tag-ids] tag-ids)
          (update-in [:parameters :form :is_hidden] boolean)
          handler))))


(defn parse-tag-inline-form-params [handler]
  (fn [req]
    (handler (update-in req [:parameters :form :is_hidden] boolean))))

;; TODO fix 500 when undefined endpoint is called
;; TODO coerction of datetime string into tick types
(def app
  (reitit.ring/ring-handler
   (reitit.ring/router
    [["/api/login" :api-login]
     ["/api/ping" :api-ping]
     ["/login" {:middleware [render-hiccup-data
                             wrap-page-hiccup-data
                             load-hiccup-template]
                :name :page-login
                :parameters {:query [:map [:location :string]]}}]
     ["" {:middleware [load-auth-from-cookie
                       authenticate]}
      ["" {:middleware [render-hiccup-data
                        wrap-page-hiccup-data
                        load-hiccup-template]}
       ["/" :page-index]
       ["/intervals" {:name :page-intervals
                      :parameters {:query [:map [:preset_id {:optional true} malli-positive-int]]}}]
       ["/intervals/{interval-id}" {:middleware [preload-interval]
                                    :name :page-interval-detail
                                    :parameters {:path [:map [:interval-id malli-positive-int]]
                                                 :query [:map [:preset_id {:optional true} malli-positive-int]]}}]
       ["/goals" :page-goals]
       ["/goals/{goal-id}" {:middleware [preload-goal-with-tags-and-intervals]
                            :name :page-goal
                            :parameters {:path [:map [:goal-id malli-positive-int]]}}]
       ["/presets" {:name :page-presets
                    :parameters {:query [:map [:search {:optional true} :string]]}}]
       ["/presets/{preset-id}" {:middleware [preload-preset-with-tags]
                                :name :page-preset
                                :parameters {:path [:map [:preset-id malli-positive-int]]
                                             :query [:map [:search {:optional true} :string]]}}]
       ["/preset-candidates/goodreads" :page-goodreads-preset-candidates]
       ["/preset-candidates/openlibrary" :page-openlibrary-preset-candidates]
       ["/timeline-overview" {:name :page-timeline-overview
                              :parameters {:query [:map
                                                   [:goal_id {:optional true} malli-positive-int]
                                                   [:start_at {:optional true} :string]
                                                   [:end_at {:optional true} :string]
                                                   [:aggregation {:enum ["yearmonthdate" "yearweek" "yearmonth"]
                                                                  :default "yearmonthdate"}
                                                    :string]]}}]]
      ["/components" {:middleware [render-hiccup-data
                                   load-hiccup-template]}
       ["/templates"
        ["/preset/form" {:name :preset-form-template
                         :middleware [parse-preset-form-params]
                         :post {:parameters
                                {:form [:map
                                        [:label :string]
                                        [:type :string]
                                        [:is_hidden {:optional true} :string]
                                        [:reference {:optional true} :string]
                                        [:tags {:optional true} :string]]}}}]
        ["/tag/inline-form" {:name :tag-form-template
                             :post {:parameters
                                    {:form [:map
                                            [:category :string]
                                            [:category_label :string]
                                            [:value :string]
                                            [:is_hidden {:optional true} :string]]}}}]]
       ["/presets/{preset-id}" {:middleware [preload-preset-with-tags]
                                :parameters {:path [:map [:preset-id malli-positive-int]]}}
        ["/card" :preset-card]
        ["/card/actions" {:name :preset-card-actions
                          :parameters {:form [:map [:name :string]]}}]
        ["/form" {:name :preset-form
                  :middleware [parse-preset-form-params]
                  :put {:parameters
                        {:form [:map
                                [:label :string]
                                [:type :string]
                                [:is_hidden {:optional true} :string]]}}}]]
       ["/tags/{tag-id}" {:middleware [preload-tag]
                          :parameters {:path [:map [:tag-id malli-positive-int]]}}
        ["/inline-form" {:name :tag-inline-form
                         :middleware [parse-tag-inline-form-params]
                         :parameters {:query [:map [:edit {:optional true} :boolean]]}
                         :put {:parameters
                               {:form [:map
                                       [:category :string]
                                       [:category_label :string]
                                       [:value :string]
                                       [:is_hidden {:optional true} :string]]}}}]]
       ["/intervals-navigation-panel" {:name :intervals-navigation-panel
                                       :parameters {:query [:map
                                                            [:preset_id {:optional true} malli-positive-int]
                                                            [:interval_id {:optional true} malli-positive-int]]}}]
       ["/intervals/{interval-id}/form" {:middleware [preload-interval]
                                         :name :clocked-interval-form
                                         :parameters {:path [:map [:interval-id malli-positive-int]]
                                                      :form [:map
                                                             [:start_at :string]
                                                             [:end_at :string]
                                                             [:timezone :string]
                                                             [:preset_id :int]]}}]
       ["/preset-candidates/openlibrary-search" {:name :openlibrary-search
                                                 :parameters {:form [:map [:query :string]]}}]
       ["/preset-candidates/goodreads-shelved" :goodreads-preset-candidates]]
      ["/data" {:middleware [render-json-data]}
       ["/intervals" :data-intervals]]]
     ["" {:get {:handler static-resource-handler}}
      ["/favicon.ico"]
      ["/resources/{*path}"]]]
    {:data {:middleware [parameters-middleware
                         wrap-format
                         wrap-cookies
                         ex-data-middleware
                         coerce-exceptions-middleware
                         coerce-request-middleware
                         assoc-routed-handler-name
                         assoc-route-substitutor
                         #(provide-db % (-> (System/getenv "DATABASE_URL")
                                            db.utilities/parse-url
                                            jdbc/get-datasource))]
            :coercion malli-coercion
            :handler handler}})
   (reitit.ring/create-default-handler)))

(defn start! [{:keys [port]}]
  (jetty/run-jetty
   (fn [req] (app req))
   {:port port :join? false}))

(defn -main []
  (start! {:port 8000}))
