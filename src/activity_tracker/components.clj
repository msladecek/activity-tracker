(ns activity-tracker.components
  (:require
   [clojure.string :as str]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   [activity-tracker.model :refer [preset-tag-groups]]
   [activity-tracker.utilities
    :refer [extend-kw split-by-day start-of-week start-of-month start-of-year ->json concat-lines]]
   [activity-tracker.external.openlibrary :as openlibrary]))


(defn- path-selected? [uri path additional-prefixes]
  (if (= "/" path)
    (= path uri)
    (some #(str/starts-with? uri %)
          (conj additional-prefixes path))))


(defn header []
  [:head
   [:meta {:charset "UTF-8"}]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
   [:title "Activity Tracker"]
   [:link {:href "/resources/node_modules/font-awesome/css/font-awesome.min.css" :rel "stylesheet"}]
   (if (System/getenv "SANDBOX")
     [:link {:href "/resources/css/custom_bulma_sandbox.css" :rel "stylesheet"}]
     [:link {:href "/resources/css/custom_bulma.css" :rel "stylesheet"}])
   [:link {:href "/resources/css/styles.css" :rel "stylesheet"}]
   [:script {:src "/resources/node_modules/hyperscript.org/dist/_hyperscript_web.min.js"}]
   [:script {:src "/resources/node_modules/htmx.org/dist/htmx.min.js"}]
   [:script {:src "https://unpkg.com/htmx-extension-error-drawer@^1/src/error-drawer.js"}]
   [:script {:src "/resources/node_modules/alpinejs/dist/cdn.js" :defer true}]
   [:script {:src "/resources/node_modules/vega/build/vega.js"}]
   [:script {:src "/resources/node_modules/vega-lite/build/vega-lite.js"}]
   [:script {:src "/resources/node_modules/vega-embed/build/vega-embed.js"}]
   [:script {:type "text/hyperscript"} "js function modulo(a, b) { return a % b; } end"]])


(defn navbar [{:keys [uri]}]
  (let [links [{:path "/" :label "Clock-in/Clock-out"}
               {:path "/intervals" :label "Clocked Intervals"}
               {:path "/goals" :label "Goals"}
               {:path "/presets" :label "Presets" :additional-prefixes #{"/preset-candidates"}}
               {:path "/timeline-overview" :label "Timeline Overview"}]]
    [:nav.navbar.is-primary
     {:role "navigation"}
     [:div.container
      [:div.navbar-brand
       [:div.navbar-item
        [:p.title "Activity Tracker"]]
       [:a.navbar-burger
        {:_ "on click toggle .is-active on me then toggle .is-active on next .navbar-menu"}
        [:span]
        [:span]
        [:span]]]
      [:div.navbar-menu
       [:div.navbar-start
        (for [{:keys [path label additional-prefixes]} links
              :let [element-tag
                    (cond-> :a.navbar-item
                      (path-selected? uri path additional-prefixes) (extend-kw :.is-active))]]
          [element-tag {:href path} label])]]]]))


(defn page [content req]
  (list
   (header)
   [:body
    {:hx-boost "true"
     :hx-ext "error-drawer"}
    (when (not= "/login" (:uri req))
      (navbar req))
    content]))


(defn oob-swap [component]
  (let [id-tag (get-in component [1 :id])]
    (update-in component [1] assoc :hx-swap-oob (format "outerHTML:#%s" id-tag))))


(defn currently-clocking [{:clocked_interval/keys [active_during]}]
  (let [label "Currently clocking:"
        start (:tick/beginning active_during)
        start-ts (.toEpochMilli start)
        hs (str "on load repeat forever\n"
                "make a Date called now\n"
                "get now.getTime()\n"
                "set now_ts to it\n"
                (format "set seconds to ((now_ts - %d) / 1000)\n" start-ts)
                (str (format "put '%s '" label)
                     "+ String(Math.floor(seconds / 60 / 60)).padStart(2, '0') "
                     "+ ':' "
                     "+ String(modulo(Math.floor(seconds / 60), 60)).padStart(2, '0') "
                     "+ ':' "
                     "+ String(Math.floor(modulo(seconds, 60))).padStart(2, '0') "
                     "into me.innerHTML\n")
                "wait 500ms")]
    [:div {:_ hs} (format "%s HH:mm:ss" label)]))


(defn last-clocked [{:clocked_interval/keys [active_during]}]
  (if active_during
    (let [start-ts (.toEpochMilli (:tick/beginning active_during))
          end-ts (.toEpochMilli (:tick/end active_during))
          hours-clocked (/ (- end-ts start-ts) 1000 60 60)
          minutes-clocked (mod (/ (- end-ts start-ts) 1000 60) 60)
          clocked-str (format "%02d:%02d" (int hours-clocked) (int minutes-clocked))
          hs (str "on load repeat forever\n"
                  "make a Date called now\n"
                  "get now.getTime()\n"
                  "set now_ts to it\n"
                  (format "set minutes to ((now_ts - %d) / 1000 / 60)\n" end-ts)
                  "if minutes > (24 * 60)\n"
                  "  then set ago_str to Math.floor(minutes / 24 / 60) + ' days'\n"
                  "  else\n"
                  "    if minutes > 60\n"
                  "      then set ago_str to Math.floor(minutes / 60) + ' hours'\n"
                  "      else set ago_str to Math.floor(minutes) + ' minutes'\n"
                  "    end\n"
                  "end\n"
                  (format "put 'Last clocked: %s ' + ago_str + ' ago' into me.innerHTML\n" clocked-str)
                  "wait 5s")]
      [:div {:_ hs} (format "Last clocked: %s" clocked-str)])
    [:div "No records yet"]))


(defn tag-card
  ([tag] (tag-card tag {}))
  ([{:tag/keys [id is_active category_label value] preset-id :preset/id}
    {:keys [mixin] :or {mixin :.is-large}}]
   (let [element (extend-kw :span.tag mixin)]
     [:div.control (when preset-id {:id (format "preset-tag-%s-%s" preset-id id)})
      [:div.tags.has-addons
       [(extend-kw element :.has-background-info-light) category_label]
       [(extend-kw element (if is_active
                             :.has-background-info.has-text-light
                             :.has-background-info-light))
        value]]])))


(defn preset-tag [{:tag/keys [id value is_active], preset-id :preset/id}]
  (when id
    [(extend-kw :span.tag.is-large.has-border
                (if is_active
                  :.has-background-info.has-text-light
                  :.has-background-info-light))
     {:id (format "preset-tag-%s-%s" preset-id id)}
     value]))


(defn preset-tags [tags]
  [:div.control
   [:div.tags.has-addons.is-flex.is-flex-wrap-nowrap
    (map preset-tag tags)]])


(defn preset-card [{:preset/keys [id label is_active tags latest_interval] :as preset}]
  (let [{layout-tags :layout non-layout-tags :non-layout}
        (preset-tag-groups tags preset)]
    [:div.card.m-3
     [:header.card-header
      [(cond-> :p.card-header-title
         is_active (extend-kw :.has-background-info.has-text-light))
       label]]
     [:div.card-content
      [:div.field
       {:style {:overflow-y "hidden"
                :overflow-x "auto"}}
       (preset-tags (map #(assoc % :preset/id id) layout-tags))]
      (when (seq non-layout-tags)
        [:div.field.is-horizontal
         [:div.field-label
          [:label.label "Other tags"]]
         [:div.field-body
          [:div.field.is-grouped.is-grouped-multiline
           (map #(tag-card (assoc % :preset/id id)
                           {:mixin :.is-medium})
                non-layout-tags)]]])
      (if is_active
        (currently-clocking latest_interval)
        (last-clocked latest_interval))]
     [:footer.card-footer
      [:button.button.is-large.card-footer-item.m-1
       {:hx-post ^:route [:preset-card-actions {:preset-id id}]
        :hx-vals (format "{\"name\": \"%s\"}" (if is_active "clock-out" "clock-in"))
        :hx-target "closest .card"
        :hx-swap "outerHTML"}
       (if is_active "Clock-out" "Clock-in")]]]))


(defn goal-summary
  ([goal] (goal-summary goal {}))
  ([{:goal/keys [value_hours window_type]}
    {:keys [title-class] :or {title-class :.title}}]
   (let [value-str (str value_hours " " (if (= 1 value_hours) "hour" "hours"))]
     [(extend-kw :p title-class)
      (format "%s every %s" value-str window_type)])))


(defn goals-navigation-panel
  ([goals] (goals-navigation-panel goals {}))
  ([goals {:keys [selected-id]}]
   [:div.panel
    (for [{goal-id :goal/id :as goal} goals
          :let [element-tag
                (cond-> :a.panel-block
                  (= selected-id goal-id)
                  (extend-kw :.has-background-grey-lighter))]]
      [element-tag
       {:href (format "/goals/%d" goal-id)}
       [:div
        (tag-card goal)
        (goal-summary goal {:title-class :.subtitle})]])]))


(defn- format-minutes [minutes]
  (format "%d:%02d"
          (int (/ minutes 60))
          (int (mod minutes 60))))


(defn- total-minutes [records]
  (reduce (fn [total interval]
            (+ total (t/minutes (t/duration interval))))
          0
          records))


(defmulti goal-metrics :goal/window_type)

(defmethod goal-metrics "day"
  [{:goal/keys [value_hours intervals]}]
  (let [dailies (->> intervals
                     (map :clocked_interval/active_during)
                     (mapcat split-by-day))
        today (filter #(= (t/date (t/zoned-date-time))
                          (t/date (t/zoned-date-time (:tick/beginning %))))
                      dailies)
        yesterday (filter #(= (t/date (t/<< (t/zoned-date-time) (t/new-duration 1 :days)))
                              (t/date (t/zoned-date-time (:tick/beginning %))))
                          dailies)
        this-week (filter #(= (start-of-week (t/now))
                              (start-of-week (:tick/beginning %)))
                          dailies)
        last-week (filter #(= (start-of-week (t/<< (t/now) (t/new-duration 7 :days)))
                              (start-of-week (:tick/beginning %)))
                          dailies)
        last-seven-days (filter #(< (t/days (t/duration (t.i/new-interval (t/date (:tick/beginning %)) (t/date (t/now)))))
                                     7)
                                dailies)
        this-month (filter #(= (start-of-month (t/now))
                               (start-of-month (:tick/beginning %)))
                           dailies)
        this-year (filter #(= (start-of-year (t/now))
                              (start-of-year (:tick/beginning %)))
                          dailies)
        days-passed-this-week (.getValue (t/day-of-week))
        days-passed-this-month (.getDayOfMonth (t/date (t/now)))
        days-passed-this-year (.getDayOfYear (t/date (t/now)))
        total-days-this-month (.lengthOfMonth (t/date (t/now)))
        total-days-this-year (.lengthOfYear (t/date (t/now)))]
    [{:label "Today"
      :value-minutes (total-minutes today)
      :goal-minutes (* 60 value_hours)}
     {:label "Yesterday"
      :value-minutes (total-minutes yesterday)
      :goal-minutes (* 60 value_hours)}
     {:label "This Week"
      :value-minutes (total-minutes this-week)
      :goal-minutes (* days-passed-this-week 60 value_hours)}
     {:label "This Week (Average)"
      :value-minutes (/ (total-minutes this-week) days-passed-this-week)
      :goal-minutes (* 60 value_hours)}
     {:label "Last Seven Days"
      :value-minutes (total-minutes last-seven-days)
      :goal-minutes (* 7 60 value_hours)}
     {:label "Last Week"
      :value-minutes (total-minutes last-week)
      :goal-minutes (* 7 60 value_hours)}
     {:label "Last Week (Average)"
      :value-minutes (/ (total-minutes last-week) 7)
      :goal-minutes (* 60 value_hours)
      :window-open? false}
     {:label "This Month"
      :value-minutes (total-minutes this-month)
      :goal-minutes (* 60 value_hours total-days-this-month)}
     {:label "This Month (Average)"
      :value-minutes (/ (total-minutes this-month) days-passed-this-month)
      :goal-minutes (* 60 value_hours)}
     {:label "This Year"
      :value-minutes (total-minutes this-year)
      :goal-minutes (* 60 value_hours total-days-this-year)}
     {:label "This Year (So far)"
      :value-minutes (total-minutes this-year)
      :goal-minutes (* 60 value_hours days-passed-this-year)}
     {:label "This Year (Average)"
      :value-minutes (/ (total-minutes this-year) days-passed-this-year)
      :goal-minutes (* 60 value_hours)}]))


(defmethod goal-metrics "week"
  [{:goal/keys [value_hours intervals]}]
  (let [dailies (->> intervals
                     (map :clocked_interval/active_during)
                     (mapcat split-by-day))
        this-week (filter #(= (start-of-week (t/now))
                              (start-of-week (:tick/beginning %)))
                          dailies)
        last-week (filter #(= (start-of-week (t/<< (t/now) (t/new-duration 7 :days)))
                              (start-of-week (:tick/beginning %)))
                          dailies)
        days-passed-this-week (.getValue (t/day-of-week))]
    [{:label "This Week"
      :value-minutes (total-minutes this-week)
      :goal-minutes (* 60 value_hours)}
     {:label "This Week (Average)"
      :value-minutes (/ (total-minutes this-week) days-passed-this-week)
      :goal-minutes (* 60 value_hours 1/7)}
     {:label "Last Week"
      :value-minutes (total-minutes last-week)
      :goal-minutes (* 60 value_hours)}
     {:label "Last Week (Average)"
      :value-minutes (/ (total-minutes last-week) 7)
      :goal-minutes (* 60 value_hours 1/7)
      :window-open? false}]))


(defn goal-table-row [{:keys [label value-minutes goal-minutes]}]
  [:tr
   {}
   [:th label]
   [:td (format-minutes value-minutes)]
   [:td (format-minutes goal-minutes)]
   [:td [(extend-kw :progress.progress
                    (if (<= goal-minutes value-minutes)
                      :.is-success
                      :.is-info))
         {:value value-minutes
          :max  goal-minutes}
         value-minutes]]])


(defn goal-table
  ([goal] (goal-table goal {}))
  ([goal {:keys [num-always-visible-metrics] :or {num-always-visible-metrics 5}}]
   (let [metrics (goal-metrics goal)
         always-visible (take num-always-visible-metrics metrics)
         conditionally-visible (drop num-always-visible-metrics metrics)]
     [:table.table.is-fullwidth
      {:x-data "{show_all: false}"}
      [:thead
       [:tr [:th] [:th "Attained"] [:th "Goal"] [:th "Progress"]]]
      (when (seq conditionally-visible)
        [:tfoot
         [:tr
          [:td {:colspan 4}
           [:button.button.is-fullwidth
            {"@click" "show_all = !show_all"
             :x-text (format "show_all ? 'Show less' : 'Show more (%d items)'"
                             (count conditionally-visible))}]]]])
      [:tbody
       (concat
        (map goal-table-row always-visible)
        (map (fn [row] (-> (goal-table-row row)
                           (assoc-in [1 :x-show] "show_all")))
             conditionally-visible))]])))


(defn goal-details [goal]
  [:div.box.section.goal-details
   [:div.title (tag-card goal)]
   (goal-summary goal)
   (goal-table goal)])


(defn interval-card
  ([interval] (interval-card interval {}))
  ([{:clocked_interval/keys [id active_during]}
    {:keys [preferred-timezone] :or {preferred-timezone "Europe/Prague"}}]
   (let [beginning (:tick/beginning active_during)
         end (:tick/end active_during)
         timestamp-fmt "YYYY-MM-dd HH:mm"
         timestamp-display-fmt "H:mm d MMM YYY"
         zoned-start (t/in (t/zoned-date-time beginning) preferred-timezone)
         zoned-end (t/in (t/zoned-date-time (or end t/now)) preferred-timezone)]
     [:p
      {:id (format "interval-card-%d" id)}
      [:b
       [:time
        {:datetime (.toString (t/duration active_during))}
        (format-minutes (t/minutes (t/duration active_during)))]]
      [:em " from "]
      [:time
       {:datetime (t/format timestamp-fmt zoned-start)}
       (t/format timestamp-display-fmt zoned-start)]
      [:em " to "]
      [:time
       {:datetime (t/format timestamp-fmt zoned-end)}
       (t/format timestamp-display-fmt zoned-end)]])))


(defn intervals-navigation-panel
  ([intervals presets] (intervals-navigation-panel intervals presets {}))
  ([intervals
    presets
    {:keys [selected-preset-id selected-interval-id preferred-timezone]
     :or {preferred-timezone "Europe/Prague"}}]
   [:div.panel
    {:hx-get ^:route [:intervals-navigation-panel
                      {}
                      (cond-> {}
                        selected-preset-id (assoc :preset_id selected-preset-id)
                        selected-interval-id (assoc :interval_id selected-interval-id))]
     :hx-trigger "reload-intervals-navigation-panel from:body"}
    [:div.panel-tabs.is-flex.is-flex-wrap-wrap
     [(cond-> :a (nil? selected-preset-id) (extend-kw :.is-active))
      {:href ^:route [:page-intervals]}
      "All"]
     (for [{:preset/keys [label id]} presets]
       [(cond-> :a (= id selected-preset-id) (extend-kw :.is-active))
        {:href ^:route [:page-intervals {} {:preset_id id}]}
        label])]
    (for [{interval-id :clocked_interval/id label :preset/label :as interval} intervals
          :let [element-tag
                (cond-> :a.panel-block
                  (= selected-interval-id interval-id)
                  (extend-kw :.has-background-grey-lighter))]]
      [element-tag
       {:href ^:route [:page-interval-detail
                       {:interval-id interval-id}
                       (when selected-preset-id
                         {:preset_id selected-preset-id})]}
       [:div
        (when-not selected-preset-id
          [:p label])
        (interval-card interval {:preferred-timezone preferred-timezone})]])]))


(defn interval-detail
  ([interval presets] (interval-detail interval presets {}))
  ([{:clocked_interval/keys [id active_during] current-preset-id :preset/id}
    presets
    {:keys [preferred-timezone] :or {preferred-timezone "Europe/Prague"}}]
   (let [trim-timestamp (fn [timestamp] (-> timestamp (t/truncate :minutes) (t/in preferred-timezone) t/date-time))
         start-at (-> active_during :tick/beginning trim-timestamp)
         end-at (-> active_during :tick/end trim-timestamp)
         is-valid (t/< start-at end-at)]
     [:form.box
      {:_ (->> ["on load or change"
                "if #start-at-field.value < #end-at-field.value"
                "  then"
                "    add [@hidden] to #end-before-start-error"
                "    remove [@disabled] from #update-interval-button"
                "  else"
                "    remove [@hidden] from #end-before-start-error"
                "    add [@disabled] to #update-interval-button"
                "end"]
               (interpose "\n")
               (apply str))}
      [:input {:hidden true :name "timezone" :value preferred-timezone}]
      [:div.field
       [:label.label "Start at"]
       [:div.field.has-addons
        [:div.control
         [:input.input
          {:id "start-at-field"
           :type "datetime-local"
           :name "start_at"
           :value start-at}]]
        [:p.control [:a.button.is-static preferred-timezone]]]]
      [:div.field
       [:label.label "End at"]
       [:div.field.has-addons
        [:div.control
         [:input.input
          {:id "end-at-field"
           :type "datetime-local"
           :name "end_at"
           :value end-at}]]
        [:p.control [:a.button.is-static preferred-timezone]]]]
      [:div.field
       [:label.label "Preset"]
       [:div.control
        [:div.select
         [:select {:name "preset_id"}
          (for [{preset-id :preset/id label :preset/label} presets]
            [:option (cond-> {:value (str preset-id)}
                       (= preset-id current-preset-id) (assoc :selected true))
             label])]]]]
      [:div.field.message.is-danger
       {:id "end-before-start-error"
        :hidden is-valid}
       [:p.message-body "`end_at` cannot precede `start_at`"]]
      [:div.field.is-grouped
       [:div.control
        [:button.button.is-warning
         {:id "update-interval-button"
          :hx-put ^:route [:clocked-interval-form {:interval-id id}]
          :hx-target "closest .box"
          :hx-swap "outerHTML"
          :disabled (not is-valid)}
         "Update"]]
       [:div.control
        [:button.button.is-danger
         {:hx-delete ^:route [:clocked-interval-form {:interval-id id}]
          :hx-confirm "Delete interval?"}
         "Delete"]]]])))


(defn clocked-time-chart [data-spec {:keys [goals tags]}]
  (let [goal-selector-id "clocked-time-chart-goal-select"
        interval-start-at-id "clocked-time-interval-start-at"
        interval-end-at-id "clocked-time-interval-end-at"
        aggregation-selector-id "clocked-time-aggregation-selector"
        tag-category-selector-id "clocked-time-tag-category-selector"
        tag-value-selector-id "clocked-time-tag-value-selector"
        tag-value-option-class "tag-value-selector-option"
        spec
        {:data (:intervals data-spec)
         :config {:customFormatTypes true}
         :height 200
         :width "container"
         :params
         [{:name "goals"
           :value (into {} (map (juxt :goal/id identity) goals))}
          {:name "tags"
           :value (into {} (map (juxt :tag/id identity) tags))}
          {:name "selected_goal_params"
           :bind {:element (format "#%s" goal-selector-id)}}
          {:name "selected_goal"
           :expr "parseInt(split(selected_goal_params, ';')[0])"}
          {:name "selected_goal_daily_hours"
           :expr "parseInt(split(selected_goal_params, ';')[1])"}
          {:name "goal_raw_minutes"
           :expr "goals[selected_goal]['goal/value_hours'] * 60 / if(goals[selected_goal]['goal/window_type'] == 'week', 7, 1)"}
          {:name "display_window_start_at"
           :bind {:element (format "#%s" interval-start-at-id)}}
          {:name "display_window_end_at"
           :bind {:element (format "#%s" interval-end-at-id)}}
          {:name "aggregation_params"
           :bind {:element (format "#%s" aggregation-selector-id)}}
          {:name "tag_category"
           :bind {:element (format "#%s" tag-category-selector-id)}}
          {:name "aggregation_window_type"
           :expr "split(aggregation_params, ';')[0]"}
          {:name "daily_goal_factor_by_aggregation_window_type"
           :value {"yearmonthdate" 1 "yearweek" 7 "yearmonth" 30.4}}
          {:name "goal_minutes"
           :expr "goal_raw_minutes * daily_goal_factor_by_aggregation_window_type[aggregation_window_type]"}
          {:name "aggregation_daily_goal_factor"
           :expr "daily_goal_factor_by_aggregation_window_type[aggregation_window_type]"}]
         :transform
         [{:calculate "ceil((time(datum.end_at) - time(datum.start_at)) / 1000 / 60)"
           :as "duration_minutes"}
          {:filter "datum.duration_minutes > 1"}
          {:filter "indexof(datum.goal_ids || [], selected_goal) != -1"}
          {:filter {:or [{:and ["datum.start_at >= display_window_start_at"
                                "datum.end_at <= display_window_end_at"]}
                         {:and ["datum.start_at <= display_window_start_at"
                                "datum.end_at >= display_window_start_at"]}
                         {:and ["datum.start_at <= display_window_end_at"
                                "datum.end_at >= display_window_end_at"]}]}}
          {:flatten ["tag_ids"] :as ["tag_id"]}
          {:calculate "tags[datum.tag_id]" :as "tag"}
          {:aggregate
           [{:op "mean" :field "duration_minutes" :as "duration_minutes"}
            {:op "min" :field "start_at" :as "start_at"}
            {:op "max" :field "end_at" :as "end_at"}
            {:op "min" :field "tag_id" :as "tag_id"}
            {:op "values" :as "agg_values"}]
           :groupby ["id" "sub_id"]}
          {:calculate "indexof(pluck(datum.agg_values, 'tag.tag/category'), tag_category)"
           :as "relevant_tag_index"}
          {:calculate "if(datum.relevant_tag_index == -1, {'tag': {'tag/value': 'N/A'}}, datum.agg_values[datum.relevant_tag_index]).tag['tag/value']"
           :as "tag_value"}
          {:calculate "datum.start_at"
           :as "date"}]
         :layer
         [{:data {:values [{:placeholder true}]}
           :transform
           [{:calculate (format "timeSequence('day', %s, %s, 1)"
                                "timeOffset('hours', datetime(display_window_start_at), -hours(display_window_start_at))"
                                "timeOffset('hours', datetime(display_window_end_at), -hours(display_window_end_at) + 24)")
             :as "dates"}
            {:flatten ["dates"] :as ["date"]}
            {:calculate "day(datum.date) == 6 || day(datum.date) == 0"
             :as "is_weekend"}
            {:calculate (->> ["(year(datum.date) == year(now()))"
                              "(month(datum.date) == month(now()))"
                              "(date(datum.date) == date(now()))"]
                             (interpose " && ")
                             (apply str))
             :as "is_today"}]
           :encoding
           {:x {:field "date"
                :timeUnit "yearmonthdate"
                :type "temporal"}
            :color {:condition
                    {:test "datum.is_today"
                     :value "red"}
                    :value "grey"}
            :opacity {:condition
                      {:test "datum.is_weekend || datum.is_today"
                       :value 0.2}
                      :value 0}
            :tooltip [{:field "date" :type "temporal"}
                      {:field "is_weekend" :type "nominal"}
                      {:field "is_today" :type "nominal"}]}
           :mark {:type "rect"
                  :tooltip true}}
          {:params
           [{:name "select"
             :select {:type "point"
                      :fields ["tag_value"]}}]
           :encoding
           {:color {:field "tag_value"
                    :type "nominal"
                    :legend {:orient "bottom"
                             :direction "vertical"}}
            :fillOpacity {:condition {:param "select" :value 1}
                          :value 0.3}
            :x {:field "start_at"
                :timeUnit "yearmonthdate"
                :type "temporal"
                :axis {:title "Date"
                       :labelAlign "left"
                       :tickCount "day"}}
            :y {:field "duration_minutes"
                :aggregate "sum"
                :type "quantitative"
                :axis {:title "Clocked time"
                       :formatType "hours_minutes_format"
                       :tickCount {:expr "(domain('y')[1] - domain('y')[0]) / 15 + 1"}
                       :values {:expr "sequence(60 * floor(domain('y')[0] / 60), 60 * (ceil(domain('y')[1] / 60) + 1), 15)"}}}
            :tooltip
            [{:field "date"
              :title "Date"
              :type "temporal"}
             {:field "duration_minutes"
              :title "Duration"
              :type "quantitative"
              :formatType "hours_minutes_format"}
             {:field "tag_value"
              :title "Tag value"
              :type "nominal"}
             {:field "agg_values[0].preset_label"
              :title "Preset label"
              :type "nominal"}
             {:field "agg_values"
              :title "Tags"
              :type "nominal"
              :formatType "tags_format"}]}
           :mark {:type "bar"
                  :band 0.95
                  :cornerRadiusEnd 10}}
          {:encoding {:y {:datum {:expr "goal_minutes"}
                          :type "quantitative"}}
           :mark "rule"}]}]
    (list
     [:div.field.is-horizontal
      [:div.field-label.is-normal
       [:label.label "Selected goal"]]
      [:div.field-body
       [:div.field
        [:div.control
         [:div.select
          [:select
           {:id goal-selector-id
            :name "goal_id"}
           (for [{goal-id :goal/id
                  tag-id :tag/id
                  goal-hours :goal/value_hours
                  goal-window :goal/window_type
                  tag-label :tag/value}
                 goals]
             (let [suffix (if (= 1 goal-hours) "" "s")
                   daily-goal-hours (case goal-window
                                      "day" goal-hours
                                      "week" (/ goal-hours 7))]
               [:option
                {:value (format "%d;%.2f" goal-id daily-goal-hours)
                 :goal_id goal-id
                 :tag_id tag-id}
                (format "%s, %.2f hour%s a %s" tag-label goal-hours suffix goal-window)]))]]]]]]
     [:div.field.is-horizontal
      [:div.field-label.is-normal
       [:label.label "Display interval"]]
      [:div.field-body
       [:div.field.has-addons
        [:p.control
         [:input.input
          {:id interval-start-at-id
           :required true
           :type "date"
           :name "start_at"
           :value (str (t/first-day-of-month (t/date)))}]]
        [:p.control
         [:a.button.is-static
          [:span.icon
           [:i.fa.fa-arrow-right]]]]
        [:p.control
         [:input.input
          {:id interval-end-at-id
           :required true
           :type "date"
           :name "end_at"
           :value (str (t/last-day-of-month (t/date)))}]]]]]
     [:div.field.is-horizontal
      [:div.field-label.is-normal
       [:label.label "Aggregation window"]]
      [:div.field-body
       [:div.field.has-addons
        [:p.control
         [:div.select
          [:select
           {:id aggregation-selector-id
            :_ "on change send reload to #timeline-chart"}
           [:option {:value "yearmonthdate"} "Day"]
           [:option {:value "yearweek"} "Week"]
           [:option {:value "yearmonth"} "Month"]]]]]]]
     [:div.field.is-horizontal
      [:div.field-label.is-normal
       [:label.label "Tag category"]]
      [:div.field-body
       [:p.control
        [:div.select
         [:select
          {:id tag-category-selector-id
           :_ (->> ["on load or change"
                    (format "repeat for el in .%s" tag-value-option-class)
                    "if el.attributes.category.value == event.target.value"
                    "then set el.hidden to false"
                    "else set el.hidden to true"
                    "end"
                    (format "set {value: 'default'} on #%s" tag-value-selector-id)]
                   (interpose "\n")
                   (apply str))}
          (for [{:tag/keys [category category_label]}
                (->> tags
                     (map #(select-keys % [:tag/category :tag/category_label]))
                     (into #{})
                     (sort-by :tag/category_label))]
            [:option {:value category} category_label])]]]]]
     [:script#timeline-chart-script {:type "text/javascript"}
      (->> ["if (typeof duration_fmt_func !== 'function') {"
            "    const duration_fmt_func = (datum, params) => ("
            "        \"\" + Math.floor(datum / 60) + \":\" + String(datum % 60).padStart(2, '0')"
            "    );"
            "    vega.expressionFunction('hours_minutes_format', duration_fmt_func);"
            "};"
            "if (typeof tags_repr_fmt_func !== 'function') {"
            "    const tags_repr_fmt_func = (datum, params) => {"
            "        var tags = datum;"
            "        tags.sort((a, b) => a.tag.category < b.tag.category);"
            "        return tags.map(value => '' + value.tag['tag/category_label'] + ': ' + value.tag['tag/value']).join(' / ')"
            "    };"
            "    vega.expressionFunction('tags_format', tags_repr_fmt_func);"
            "};"]
           (interpose "\n")
           (apply str))]
     [:div#timeline-chart.is-flex.is-flex-direction-column-reverse
      {:_ (->> ["on load or reload"
                (format "set spec to %s" (->json spec))
                (format "get value of #%s" aggregation-selector-id)
                "set timeunit to it"
                "js (spec, timeunit)"
                "spec['layer'][1]['encoding']['x']['timeUnit'] = timeunit;"
                "vegaEmbed('#timeline-chart', spec, {actions: false});"
                "end"]
               (interpose "\n")
               (apply str))}
      {:style {:width "100%"}}])))

(defn preset-navigation-panel
  ([presets] (preset-navigation-panel presets {}))
  ([presets {:keys [selected-id current-handler-id search]}]
   [:div.panel
    {:x-data (format "{search: '%s'}" (or search ""))}
    [:div.panel-block
     [:p.control.has-icons-left
      [:input.input
       {:name "search"
        :type "text"
        :value (or search "")
        :placeholder "Search"
        :x-model "search"}]
      [:span.icon.is-left
       [:i.fa.fa-search]]]
     [:button.hide-search-button.button
      {:_ "on click toggle .hidden on .preset-card"}
      "Toggle Search Results"]]
    (for [{:preset/keys [id label is_hidden]} presets
          :let [element-tag
                (cond-> :div.panel-block.is-justify-content-space-between.preset-card
                  (= selected-id id) (extend-kw :.has-background-grey-lighter))]]
      [element-tag
       {:x-show (format "\"%s\".toLowerCase().includes(search.toLowerCase())" label)}
       [:p label]
       [:p.is-flex.is-align-items-center
        (when is_hidden
          [:span.icon.m-2
           [:i.fas.fa-eye-slash]])
        [(cond-> :a.button
           (= selected-id id) (extend-kw :.is-static))
         {:href ^:route [:page-preset {:preset-id id}]
          :hx-include "[name='search']"}
         "Detail/Edit"]]])]))


(defn tag-editor-row-edit [{:tag/keys [id category category_label is_hidden value]}]
  ;; TODO get rid of the hidden id cell (add it somewhere else in the form)
  (let [form-id (format "tag-row-edit-%s" (or (str id) (str "initial-" (rand-int 10000))))]
    [:tr
     [:td [:input.input
           {:type "text"
            :value category
            :name "category"
            :form form-id}]]
     [:td [:input.input
           {:type "text"
            :value category_label
            :name "category_label"
            :form form-id}]]
     [:td [:input.input
           {:type "text"
            :value value
            :name "value"
            :form form-id}]]
     [:td [:input
           {:type "checkbox"
            :value (str is_hidden)
            :checked is_hidden
            :name "is_hidden"
            :form form-id}]]
     [:td
      [:input
       {:hidden true
        :name "id"
        :value id
        :form form-id}]
      [:form
       {:id form-id}
       [:div.buttons.has-addons
        [:button.button.is-warning
         (merge
          {:type "button"
           :hx-target "closest tr"
           :hx-swap "outerHTML"}
          (if (nil? id)
            {:hx-post ^:route [:tag-form-template]}
            {:hx-put  ^:route [:tag-inline-form {:tag-id id}]}))
         [:span.icon.is-small
          [:i.fa.fa-save]]]
        [:button.button
         (merge
          {:type "button"}
          (if (nil? id)
            {:_ "on click remove closest <tr/>"}
            {:hx-get ^:route [:tag-inline-form {:tag-id id} {:edit false}]
             :hx-target "closest tr"
             :hx-swap "outerHTML"}))
         [:span.icon.is-small
          [:i.fa.fa-times]]]]]]]))


(defn tag-editor-row-view [{:tag/keys [id category category_label is_hidden value]}]
  [:tr
   [:td category]
   [:td category_label]
   [:td value]
   [:td (str is_hidden)]
   [:td
    [:button.button
     {:type "button"
      :hx-get ^:route [:tag-inline-form {:tag-id id} {:edit true}]
      :hx-target "closest tr"
      :hx-swap "outerHTML"}
     "Edit"]]])


(defn tag-editor-modal [tags]
  [:div.modal
   {:id "tag-edit-modal"}
   [:div.modal-background]
   [:div.container
    {:style {:overflow-y "auto"}}
    [:header.modal-card-head
     [:p.modal-card-title "Edit Tags"]
     [:button.delete
      {:type "button"
       :_ "on click remove .is-active from #tag-edit-modal"}]]
    [:section.modal-card-body
     [:table#tag-edit-table.table
      [:thead
       [:tr
        [:th "Category"]
        [:th "Category Label"]
        [:th "Value"]
        [:th "Is Hidden"]
        [:th "Actions"]]]
      [:tbody
       (->> tags
            (sort-by (juxt :tag/category :tag/value))
            (map tag-editor-row-view))]]
     [:button.button.is-fullwidth
      {:type "button"
       :hx-get ^:route [:tag-form-template]
       :hx-target "#tag-edit-table > tbody"
       :hx-swap "beforeend"}
      "Add"]]
    [:footer.modal-card-foot
     [:button.button
      {:_ "on click remove .is-active from #tag-edit-modal"}
      "Close"]]]])


(defn tag-selector-tag-label [{:tag/keys [id category_label value]}]
  [:span
   {:id (format "tag-selector-panel-tag-%d" id)}
   (format "%s / %s" category_label value)])


(defn tag-selector-tag-block [{:keys [selected-tag-ids]} tag]
  [:div.panel-block
   {}
   [:label.checkbox
    [:input
     {:type "checkbox"
      :checked (selected-tag-ids (:tag/id tag))
      :name (format "tag-%d" (:tag/id tag))}]
    (tag-selector-tag-label tag)]])


(defn tag-selector
  ([tags] (tag-selector {} tags))
  ([{:keys [selected-tag-ids] :or {selected-tag-ids #{}}} tags]
   (list
    [:div.field
     [:label.label "Tags"]
     [:div.control
      [:div.panel
       {:style {:overflow-y "scroll"
                :height "10em"}}
       (->> tags
            (sort-by (juxt :tag/category :tag/value))
            (map #(tag-selector-tag-block {:selected-tag-ids selected-tag-ids} %)))
       [:div#tag-selector-button-block.panel-block
        [:button.button.is-fullwidth
         {:type "button"
          :_ "on click add .is-active to #tag-edit-modal"}
         "Edit Tags"]]]]]
    (tag-editor-modal tags))))


(defn preset-form [selected-preset {:keys [tags]}]
  (let [selected-preset-tags (->> (:preset/tags selected-preset)
                                  (map :tag/id)
                                  (into #{}))]
    [:form
     [:div.field
      [:label.label "Label"]
      [:div.control
       [:input.input
        {:name "label"
         :required true
         :type "text"
         :value (:preset/label selected-preset)}]]]
     [:div.field
      [:label.label "Type"]
      [:div.control
       [:input.input
        {:name "type"
         :required true
         :type "text"
         :value (:preset/type selected-preset)}]]]
     (tag-selector
      {:selected-tag-ids selected-preset-tags}
      tags)
     [:div.field
      [:div.control
       [:label.checkbox
        [:input
         {:type "checkbox"
          :name "is_hidden"
          :checked (:preset/is_hidden selected-preset)}]
        " Is hidden"]]]
     [:div.control
      [:button.button.is-primary.is-fullwidth
       (merge
        {:type "button"
         :hx-swap "none"}
        (if (nil? (:preset/id selected-preset))
          {:hx-post ^:route [:preset-form-template]}
          {:hx-put ^:route [:preset-form {:preset-id (:preset/id selected-preset)}]}))
       "Submit"]]]))

(defn preset-action-tabs [{:keys [current-handler-id]}]
  [:div.tabs.is-toggle.is-fullwidth
   [:ul
    (for [[handler-id label] [[:page-presets "Fresh"]
                              [:page-goodreads-preset-candidates "Goodreads"]
                              [:page-openlibrary-preset-candidates "OpenLibrary"]]]
      [(cond-> :li (= handler-id current-handler-id) (extend-kw :.is-active))
       [:a {:href ^:route [handler-id]} label]])]])

(defn goodreads-book-candidates
  ([books] (goodreads-book-candidates books {}))
  ([books {:keys [existing-presets] :or {existing-presets #{}}}]
   [:div
    (for [book books]
      (let [title (:title_without_series book)
            series (if (= title (:title book))
                     nil
                     (->> (subs (:title book) (count title))
                          (re-find #"(?<=\().+(?=, #\d+\))")))
            tags (cond-> (concat
                          [{"category" "activity"
                            "category_label" "Activity"
                            "value" "Reading"}
                           {"category" "activity"
                            "category_label" "Activity"
                            "value" "Reading (books)"}
                           {"category" "subactivity"
                            "category_label" "Subactivity"
                            "value" "Reading (books)"}
                           {"category" "book_title"
                            "category_label" "Title"
                            "value" title}]
                          (for [author (:authors book)]
                            {"category" "book_author"
                             "category_label" "Author"
                             "value" author}))
                   series (conj {"category" "book_series"
                                 "category_label" "Series"
                                 "value" series}))]
        [:div.panel-block.is-flex.is-justify-content-space-between
         [:p title]
         [:button.button.is-primary
          {:disabled (contains? existing-presets (:link book))
           :hx-post ^:route [:preset-form-template]
           :hx-vals (->json {"label" title
                             "type" "book"
                             "reference" (:link book)
                             "tags" (->json tags)})
           :hx-swap "none"}
          "Create preset"]]))]))

(defn openlibrary-search-panel []
  [:div.panel
   [:p.panel-heading
    "OpenLibrary"]
   [:div.panel-block
    [:p.control
     [:input.input
      {:type "text"
       :name "query"
       :placeholder "Search"
       :hx-post ^:route [:openlibrary-search]
       :hx-trigger "keyup changed delay:500ms"
       :hx-target "#openlibrary-search-results"
       :hx-indicator "#search-loading-indicator"}]
     [:span#search-loading-indicator.htmx-indicator.panel-block
      "Loading..."]]]
   [:div#openlibrary-search-results]])

(defn openlibrary-book-candidates
  ([books] (openlibrary-book-candidates books {}))
  ([books {:keys [existing-presets] :or {existing-presets #{}}}]
   (list
    (for [book books]
      (let [title (:title book)
            authors (:author_name book)
            tags (concat
                  [{"category" "activity"
                    "category_label" "Activity"
                    "value" "Reading"}
                   {"category" "book_title"
                    "category_label" "Title"
                    "value" title}]
                  (for [author authors]
                    {"category" "book_author"
                     "category_label" "Author"
                     "value" author}))
            table-fields
            [{:label "Title" :value title}
             {:label "Author" :value (str/join ", " authors)}
             {:label "Number of pages" :value (:number_of_pages_median book)}
             {:label "Published" :value (:first_publish_year book)}
             {:label "Publisher" :value (let [joined (str/join " / " (:publisher book))]
                                          (if (< 100 (count joined))
                                            (str (subs joined 0 90) "...")
                                            joined))}]]
        [:div.panel-block
         [:article.media
          [:figure.media-left
           [:p.image
            [:img
             {:src (format "https://covers.openlibrary.org/b/id/%s-M.jpg?default=false"
                           (:cover_i book))
              :onerror "this.src='/resources/images/cover_not_found.png';"
              :width 120}]]]
          [:div.media-content
           [:div.content
            [:table.table.is-fullwidth
             (for [{:keys [label value]} table-fields
                   :when value]
               [:tr [:th label] [:td value]])]]
           [:button.button.is-primary
            {:disabled (contains? existing-presets (:link book))
             :hx-post ^:route [:preset-form-template]
             :hx-vals (->json {"label" title
                               "type" "book"
                               "reference" (:link book)
                               "tags" (->json tags)})
             :hx-swap "none"}
            "Create preset"]]]])))))

(defn preset-stats [preset]
  [:table.table.is-fullwidth
   [:tbody
    [:tr
     [:th "Total Clocked"]
     [:td (-> (:total-duration preset)
              t/minutes
              format-minutes)]]]])
