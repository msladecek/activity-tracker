(ns activity-tracker.db.methods
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   [next.jdbc :as jdbc]
   [honey.sql :as sql]
   [honey.sql.helpers :as sql-clause]
   [activity-tracker.utilities :refer [map->ns-map ns-map->map]]))



(sql/register-clause! :left-outer-join (fn [k clauses] (#'sql/format-join k clauses)) :where)


(defn- strip-first-and-last-character [string]
  (subs string 1 (dec (count string))))


(defn- parse-interval-boundary [dt-str]
  (if dt-str
    (-> dt-str
        strip-first-and-last-character
        (str/replace " " "T")
        java.time.ZonedDateTime/parse
        t/instant)
    (t/now)))


(defn- parse-clocked-interval [interval]
  (when interval
    (let [ns-interval (map->ns-map "clocked_interval" interval)
          [start-at end-at] (-> (:clocked_interval/active_during ns-interval)
                                strip-first-and-last-character
                                (str/split #","))
          active_during (t.i/new-interval (parse-interval-boundary start-at)
                                          (parse-interval-boundary end-at))]
      (-> ns-interval
          (assoc :clocked_interval/active_during active_during
                 :preset/id (:clocked_interval/preset_id interval))
          (dissoc :clocked_interval/preset_id)))))


(defn- remap-preset-with-tags [record]
  (-> record
      (assoc :preset/tags (->> (:tags record)
                               (map #(map->ns-map "tag" %) )
                               (into #{}))
             :preset/is_active (:is_active record)
             :preset/latest_interval (parse-clocked-interval (:latest_interval record)))
      (dissoc :tags)
      (dissoc :is_active)
      (dissoc :latest_interval)))


(def ^:private tag-is-active
  {:select [[[:inline 1]]]
   :from [[:preset_tag :pt]]
   :join [[:clocked_interval :ci] [:= :ci.preset_id :pt.preset_id]]
   :where [:and
           [:= :pt.tag_id :tag.id]
           [:not :ci.is_deleted]
           [:upper_inf :ci.active_during]]})


(def ^:private annotated-tag
  {:select [:tag.*
            [[:exists tag-is-active] :is_active]]
   :from :tag
   :where [:not :tag.is_hidden]})


(def ^:private latest-interval
  {:select-distinct-on [[:preset_id] :*]
   :from :clocked_interval
   :where [:not :is_deleted]
   :order-by [:preset_id [[:lower :active_during] :desc]]})


;; TODO hidden tags misbehave - they are returned but without attributes?
(def ^:private presets-with-tags
  {:with [[:annotated_tag annotated-tag]
          [:latest_interval latest-interval]]
   :select [:preset.*
            [[:json_agg :annotated_tag.*]
             :tags]
            [[:exists
              {:select [[[:inline 1]]]
               :from [[:clocked_interval :ci]]
               :where [:and
                       [:= :ci.preset_id :preset.id]
                       [:not :ci.is_deleted]
                       [:upper_inf :ci.active_during]]}]
             :is_active]
            [[:row_to_json :li]
             :latest_interval]]
   :from :preset
   :left-outer-join [[:preset_tag :pt] [:= :pt.preset_id :preset.id]
                     [:latest_interval :li] [:= :li.preset_id :preset.id]
                     :annotated_tag [:= :annotated_tag.id :pt.tag_id]]
   :group-by [:preset.id :li.*]})


(defn get-visible-presets-with-tags [db]
  (->> (sql-clause/where presets-with-tags [:not :preset.is_hidden])
       sql/format
       (jdbc/execute! db)
       (mapv remap-preset-with-tags)))

(defn get-preset-with-tags [db preset-id]
  (->> (sql/format (-> presets-with-tags
                       (sql-clause/where [:= :preset.id preset-id])))
       (jdbc/execute-one! db)
       remap-preset-with-tags))

(defn- where-preset-is-active [preset-id]
  [:and
   [:= :preset_id preset-id]
   [:not :is_deleted]
   [:upper_inf :active_during]])

(defn get-active-interval [db preset-id]
  (->> {:select [:*]
        :from :clocked_interval
        :where (where-preset-is-active preset-id)}
       sql/format
       (jdbc/execute-one! db)))


(defn insert-active-interval [db preset-id]
  (->> {:insert-into :clocked_interval
        :values [{:active_during [:tstzrange [:now] :null] :preset_id preset-id}]
        :returning :*}
       sql/format
       (jdbc/execute-one! db)))

(defn deactivate-active-intervals [db preset-id]
  (->> {:update :clocked_interval
        :set {:active_during [:tstzrange [:lower :active_during] [:now]]}
        :where (where-preset-is-active preset-id)}
       sql/format
       (jdbc/execute-one! db)))

(defn get-related-visible-tags-on-other-presets [db preset-id]
  (->> {:select [:related_preset.id
                 :tag.*
                 [[:exists
                   {:select [1]
                    :from [[:preset_tag :pt]]
                    :join [[:clocked_interval :ci] [:= :ci.preset_id :pt.preset_id]]
                    :where [:and
                            [:= :pt.tag_id :tag.id]
                            [:not :ci.is_deleted]
                            [:upper_inf :ci.active_during]]}]
                  :is_active]
                 [[:= :tag.category [:any :related_preset.layout]] :is_layout]]
        :from :tag
        :join [[:preset_tag :selector_pt] [:= :selector_pt.tag_id :tag.id]
               [:preset_tag :related_pt] [:= :related_pt.tag_id :tag.id]
               [:preset :related_preset] [:= :related_preset.id :related_pt.preset_id]]
        :where [:and
                [:= :selector_pt.preset-id preset-id]
                [:not [:= :related_pt.preset_id preset-id]]
                [:not :related_preset.is_hidden]]}
       sql/format
       (jdbc/execute! db)))

(defn get-intervals-of-non-deleted-presets [db]
  (->> {:select [:clocked_interval.* :preset.label]
        :from :clocked_interval
        :join [:preset [:= :preset.id :clocked_interval.preset_id]]
        :where [:and
                [:not :clocked_interval.is_deleted]
                [:not :preset.is_hidden]]
        :order-by [[[:lower :active_during] :desc]]}
       sql/format
       (jdbc/execute! db)
       (mapv parse-clocked-interval)))

(defn get-intervals-by-preset [db preset-id]
  (->> {:select [:clocked_interval.* :preset.label]
        :from :clocked_interval
        :join [:preset [:= :preset.id :clocked_interval.preset_id]]
        :where [:and
                [:= :preset_id preset-id]
                [:not :is_deleted]]
        :order-by [[[:lower :active_during] :desc]]}
       sql/format
       (jdbc/execute! db)
       (mapv parse-clocked-interval)))

(defn get-presets [db {:keys [include-hidden] :or {include-hidden false}}]
  (->> (cond-> {:select [:*]
                :from :preset
                :order-by [:preset.label]}
         (not include-hidden) (sql-clause/where [:not :is-hidden]))
       sql/format
       (jdbc/execute! db)))

(defn get-tagged-goals [db]
  (->> {:select [:tag.* :goal.*]
        :from :goal
        :join [:tag [:= :tag.id :goal.tag_id]]
        :where [:not :tag.is_hidden]}
       sql/format
       (jdbc/execute! db)))

(defn get-tagged-and-clocked-goal [db goal-id]
  (let [record (->> {:select [:tag.*
                              :goal.*
                              [[:filter [:json_agg :clocked_interval]
                                {:where [:and
                                         [:is-not :clocked_interval.id nil]
                                         [:not :clocked_interval.is_deleted]]}]
                               :intervals]]
                     :from :goal
                     :join [:tag [:= :tag.id :goal.tag_id]]
                     :left-outer-join [[:preset_tag :pt] [:= :pt.tag_id :goal.tag_id]
                                       :clocked_interval [:= :clocked_interval.preset_id :pt.preset_id]]
                     :where [:= :goal.id goal-id]
                     :group-by [:tag.id :goal.id]}
                    sql/format
                    (jdbc/execute-one! db))]
    (-> record
        (assoc :goal/intervals (mapv parse-clocked-interval (:intervals record)))
        (dissoc :intervals))))

(defn get-tags-of-non-hidden-presets [db]
  (->> {:select [:*]
        :from :tag
        :where [:and
                [:not :tag.is_hidden]
                [:exists {:select [[[:inline 1]]]
                          :from [:preset_tag]
                          :join [:preset [:= :preset.id :preset_tag.id]]
                          :where [:not :preset.is_hidden]}]]}
       sql/format
       (jdbc/execute! db)
       (map (fn [record]
              (-> record
                  (assoc :tag/is_active (:is_active record))
                  (dissoc :is_active))))))

(defn get-intervals-with-goals [db]
  (->> {:select [:clocked_interval.*
                 :preset.label
                 [[:filter [:array_agg :goal.id] {:where [:is-not :goal.id nil]}] :goal_ids]
                 [[:filter [:array_agg :tag.id] {:where [:is-not :tag.id nil]}] :tag_ids]]
        :from :clocked_interval
        :join [:preset [:= :preset.id :clocked_interval.preset_id]]
        :left-outer-join [[:preset_tag :pt] [:= :pt.preset_id :preset.id]
                          :tag [:= :tag.id :pt.tag_id]
                          :goal [:= :goal.tag_id :tag.id]]
        :where [:and
                [:not :clocked_interval.is_deleted]
                [:not :tag.is_hidden]]
        :group-by [:clocked_interval.id :preset.label]
        :order-by [[[:lower :clocked_interval.active_during] :desc]]}
       sql/format
       (jdbc/execute! db)
       (mapv parse-clocked-interval)))


(defn get-interval-with-preset-label [db interval-id]
  (->> {:select [:clocked_interval.* :preset.label]
        :from :clocked_interval
        :join [:preset [:= :preset.id :clocked_interval.preset_id]]
        :where [:and
                [:not :clocked_interval.is_deleted]
                [:= :clocked_interval.id interval-id]]}
       sql/format
       (jdbc/execute-one! db)
       parse-clocked-interval))


(defn get-tags [db]
  (->> {:select [:*]
        :from :tag
        :where [:not :tag.is_hidden]}
       sql/format
       (jdbc/execute! db)
       (map (fn [record]
              (-> record
                  (assoc :tag/is_active (:is_active record))
                  (dissoc :is_active))))))


(defn get-tag [db tag-id]
  (let [record (->> {:select [:*]
                     :from :tag
                     :where [:= :tag.id tag-id]}
                    sql/format
                    (jdbc/execute-one! db))]
    (-> record
        (assoc :tag/is_active (:is_active record))
        (dissoc :is_active))))

(defn update-tag [db tag-id tag-params]
  (->> {:update :tag
        :set (select-keys tag-params [:category :category_label :value :is_hidden])
        :where [:= :tag.id tag-id]}
       sql/format
       (jdbc/execute-one! db)))

(defn get-tag-by-attrs [db params]
  (->> (reduce
        (fn [query [attr value]]
          (sql-clause/where query [:= attr value]))
        {:select [:*]
         :from :tag}
        params)
       sql/format
       (jdbc/execute-one! db)))


(defn insert-tag [db tag-params]
  (->> {:insert-into :tag
        :values [tag-params]
        :returning :*}
       sql/format
       (jdbc/execute-one! db)))


(defn insert-preset [db preset-params]
  (->> {:insert-into :preset
        :values [(select-keys preset-params [:is_hidden :label :type :layout :reference])]
        :returning :*}
       sql/format
       (jdbc/execute-one! db)))


(defn insert-preset-tag [db {:keys [preset-id tag-id]}]
  (->> {:insert-into :preset_tag
        :values [{:preset_id preset-id :tag_id tag-id}]}
       sql/format
       (jdbc/execute-one! db)))


(defn delete-preset-tag [db {:keys [preset-id tag-id]}]
  (->> {:delete-from :preset_tag
        :where [:and
                [:= :preset_id preset-id]
                [:= :tag_id tag-id]]}
       sql/format
       (jdbc/execute-one! db)))


(defn update-preset [db preset-id preset-params]
  (->> {:update :preset
        :set (select-keys preset-params [:is_hidden :label :type :layout :reference])
        :where [:= :preset.id preset-id]}
       sql/format
       (jdbc/execute-one! db)))


(defn soft-delete-interval [db interval-id]
  (->> {:update :clocked_interval
        :set {:is_deleted true}
        :where [:= :clocked_interval.id interval-id]}
       sql/format
       (jdbc/execute-one! db)))


(defn update-interval [db interval-id {:keys [active_during preset_id]}]
  (->> {:update :clocked_interval
        :set {:preset_id preset_id :active_during [:cast active_during :tstzrange]}
        :where [:= :clocked_interval.id interval-id]}
       sql/format
       (jdbc/execute-one! db)))
