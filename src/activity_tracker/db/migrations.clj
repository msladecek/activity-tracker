(ns activity-tracker.db.migrations
  (:require
   [activity-tracker.db.utilities
    :refer [parse-url]]
   [migratus.core :as migratus]))


(def config
  {:store :database
   :migration-dir "migrations/"
   :migration-table-name "migratus"
   :db (parse-url (System/getenv "DATABASE_URL"))})


(defn migrate [_]
  (migratus/migrate config))
