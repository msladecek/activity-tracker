(ns activity-tracker.db.utilities
  (:import
   (org.postgresql.util PGobject)
   (java.sql PreparedStatement)
   (java.sql Array))
  (:require
   ;; [hugsql.core :as hugsql]
   ;; [hugsql.adapter.next-jdbc :as next-adapter]
   [next.jdbc.prepare :as prepare]
   [next.jdbc.result-set :as rs]
   [activity-tracker.utilities :refer [->json <-json]]))


(defn parse-url [url]
  (let [components (clojure.string/split url #"(://|:|@|/)")]
    (zipmap [:dbtype :user :password :host :port :dbname] components)))


(defn ->pgobject
  "Transforms Clojure data to a PGobject that contains the data as
  JSON. PGObject type defaults to `jsonb` but can be changed via
  metadata key `:pgtype`"
  [x]
  (let [pgtype (or (:pgtype (meta x)) "jsonb")]
    (doto (PGobject.)
      (.setType pgtype)
      (.setValue (->json x)))))


(defn <-pgobject [^org.postgresql.util.PGobject v]
  (let [type  (.getType v)
        value (.getValue v)]
    (if (#{"jsonb" "json"} type)
      (when value
        (with-meta (<-json value) {:pgtype type}))
      value)))


(set! *warn-on-reflection* true)

(extend-protocol prepare/SettableParameter
  clojure.lang.IPersistentMap
  (set-parameter [m ^PreparedStatement s i]
    (.setObject s i (->pgobject m)))

  clojure.lang.IPersistentVector
  (set-parameter [v ^PreparedStatement s i]
    (.setObject s i (->pgobject v))))


(extend-protocol rs/ReadableColumn
  Array
  (read-column-by-label [^Array v _] (vec (.getArray v)))
  (read-column-by-index [^Array v _2 _3] (vec (.getArray v)))

  org.postgresql.util.PGobject
  (read-column-by-label [^org.postgresql.util.PGobject v _] (<-pgobject v))
  (read-column-by-index [^org.postgresql.util.PGobject v _2 _3] (<-pgobject v)))
