(ns activity-tracker.external.goodreads
  (:require
   [clojure.data.xml :as xml]
   [clojure.data.zip.xml :as zip-xml]
   [clojure.zip :as zip]
   [org.httpkit.client :as http]
   [tick.core :as t]))


(def goodreads-api-url "https://www.goodreads.com")


(defn get-reviews-request
  [{:keys [shelf sort per_page page id key]
    :or {shelf "read"
         sort "date_started"
         per_page 200
         page 1
         id (System/getenv "GOODREADS_CLIENT_ID")
         key (System/getenv "GOODREADS_API_KEY")}}]
  (http/get (str goodreads-api-url "/review/list")
            {:query-params {"id" id
                            "key" key
                            "v" 2
                            "sort" sort
                            "per_page" per_page
                            "shelf" shelf
                            "page" page}}))


(defn get-reviews [options]
  (loop [{:keys [per_page page]
          :or {page 1 per_page 200}
          :as options} options
         reviews []]
    (let [data (->> @(get-reviews-request options) :body xml/parse-str zip/xml-zip)
          reviews-loc (zip-xml/xml1-> data :reviews)
          {:keys [total end]} (->> [:end :total :start]
                                   (map (fn [attr]
                                          [attr (Integer/parseInt (zip-xml/attr reviews-loc attr))]))
                                   (into {}))
          reviews-next (into reviews (zip-xml/xml-> data :reviews :review))]
      (if (< (count reviews) total)
        (recur (assoc options :page (inc page))
               reviews-next)
        reviews-next))))


(defn get-review-request
  [{:keys [user_id book_id key]
    :or {user_id (System/getenv "GOODREADS_CLIENT_ID")
         key (System/getenv "GOODREADS_API_KEY")}}]
  (http/get (str goodreads-api-url "/review/show_by_user_and_book")
            {:query-params {"user_id" user_id
                            "book_id" book_id
                            "key" key}}))


(defn get-review [options]
  (-> @(get-review-request options)
      :body
      xml/parse-str
      zip/xml-zip
      (zip-xml/xml-> :review)
      first))


(defn- str->int [string]
  (try (Integer/parseInt string)
       (catch Exception e nil)))


(def ^:private timestamp-formatter (t/formatter "E MMM dd kk:mm:ss xxxx yyyy"))


(defn- datetime-string->inst [text]
  (when-not (= "" text)
    (-> text
        (java.time.ZonedDateTime/parse timestamp-formatter)
        t/inst)))


(def ^:private review-attr-transformers
  {:started_at (comp datetime-string->inst zip-xml/text)
   :read_at (comp datetime-string->inst zip-xml/text)
   :rating (comp str->int zip-xml/text)})


(def ^:private book-attr-transformers
  {:authors #(into [] (zip-xml/xml-> % :author :name zip-xml/text))
   :id (comp str->int zip-xml/text)
   :isbn13 zip-xml/text
   :link zip-xml/text
   :published (comp str->int zip-xml/text)
   :title zip-xml/text
   :title_without_series zip-xml/text
   :num_pages (comp str->int zip-xml/text)})


(defn remap-review [review]
  (->> (concat
        (map (fn [[attr parse]]
               (let [text (zip-xml/xml1-> review attr)]
                 [attr (parse text)]))
             review-attr-transformers)
        (map (fn [[attr parse]]
               (let [text (zip-xml/xml1-> review :book attr)]
                 [attr (parse text)]))
             book-attr-transformers))
       (into {})))
