(ns activity-tracker.external.openlibrary
  (:require
   [activity-tracker.utilities :refer [<-json]]
   [org.httpkit.client :as http]))


(def openlibrary-api-url "https://openlibrary.org")


(defn get-search-request [query]
  (http/get (str openlibrary-api-url "/search.json")
            {:query-params {"q" query}}))

(defn get-search [query]
  (-> @(get-search-request query)
      :body
      <-json))
