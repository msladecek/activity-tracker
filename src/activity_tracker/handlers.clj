(ns activity-tracker.handlers
  (:require
   [clojure.string :as str]
   [activity-tracker.model :as model]
   [buddy.sign.jwt :as jwt]
   [buddy.hashers :as hashers]
   [tick.core :as t]
   [activity-tracker.external.goodreads :as goodreads]
   [activity-tracker.external.openlibrary :as openlibrary]
   [activity-tracker.utilities :as utilities]))


(defmulti data-handler
  (fn [{:keys [handler-name request-method]}]
    [handler-name request-method]))


(defmethod data-handler [:page-index :get] [{:keys [db]}]
  {:presets (model/presets-with-tags db)})


(defmethod data-handler [:page-login :get] [{:keys [parameters]}]
  {:return-location (get-in parameters [:query :location])})


(defmethod data-handler [:preset-card :get] [{:keys [preloads]}]
  (select-keys preloads [:preset]))


(defmethod data-handler [:preset-card-actions :post] [{:keys [db preloads parameters]}]
  (let [preset (:preset preloads)
        preset-id (:preset/id preset)
        action-name (get-in parameters [:form :name])
        action (case action-name
                 "clock-in" model/clock-in-preset
                 "clock-out" model/clock-out-preset
                 nil)]
    (if action
      (do
        (action db preset-id)
        {:preset (model/preset-with-tags db preset-id)
         :related-tags (model/related-tags-on-other-presets db preset-id)})
      (throw
       (ex-info "unknown preset card action"
                {:status 400
                 :title "Bad Request"
                 :preset-id preset-id
                 :detail "Unknown action"})))))


(defmethod data-handler [:page-timeline-overview :get] [{:keys [db route-substitutor parameters]}]
  {:goals (model/tagged-goals db)
   :tags (model/tags db)
   :params (:query parameters)
   :intervals-data-uri (route-substitutor ^:route [:data-intervals {} {:split "day"}])})


(defn- remap-interval
  [{:clocked_interval/keys [active_during is_deleted id goal_ids tag_ids]
    preset-id :preset/id
    preset-label :preset/label
    sub-id :sub-id}]
  {:start_at (:tick/beginning active_during)
   :end_at (:tick/end active_during)
   :preset_label preset-label
   :id id
   :is_deleted is_deleted
   :preset_id preset-id
   :goal_ids goal_ids
   :tag_ids tag_ids
   :sub_id sub-id})


(defmethod data-handler [:data-intervals :get] [{:keys [db parameters]}]
  (let [split-param (get-in parameters [:query :split])
        intervals (model/clocked-intervals-with-goals db)]
    (case split-param
      nil (map remap-interval intervals)
      "day" (->> intervals
                 (mapcat (fn [interval]
                           (for [[sub-id splat] (map vector (range) (utilities/split-by-day (:clocked_interval/active_during interval)))]
                             (assoc interval
                                    :clocked_interval/active_during splat
                                    :sub-id sub-id))))
                 (map remap-interval)))))


;; TODO get rid of this, use htmx + hx-redirect instead
;; TODO render something different on failed login
(defmethod data-handler [:api-login :post] [req]
  (let [{:strs [location password username]} (:form-params req)
        auth-secret (System/getenv "AUTH_SECRET")
        true-password-hash (System/getenv "PASSWORD_HASH")
        response-meta (if (and (= username "martin")
                               (hashers/check password true-password-hash))
                        {:status 302
                         :headers {"Location" (:location (jwt/unsign location auth-secret))}
                         :cookies {"_activity-tracker_token"
                                   {:value (jwt/sign
                                            {:user username :iat (t/now)}
                                            auth-secret)
                                    :path "/"
                                    :same-site :strict}}}
                        {:status 302
                         :headers {"Location" (str "/login" "?location=" location)}})]
    (with-meta {} response-meta)))


(defmethod data-handler [:page-intervals :get] [{:keys [db parameters]}]
  (let [preset-id (get-in parameters [:query :preset_id])]
    {:selected-preset-id preset-id
     :clocked-intervals (model/clocked-intervals db {:preset/id preset-id})
     :presets (model/presets db)}))


(defmethod data-handler [:page-interval-detail :get] [{:keys [db parameters preloads]}]
  (let [preset-id (get-in parameters [:query :preset_id])]
    {:interval (:interval preloads)
     :selected-preset-id preset-id
     :clocked-intervals (model/clocked-intervals db {:preset/id preset-id})
     :presets (model/presets db)}))


(defmethod data-handler [:page-presets :get] [{:keys [db handler-name]}]
  {:presets (model/presets db {:include-hidden true})
   :tags (model/tags db)
   :current-handler-id handler-name})


(defmethod data-handler [:page-preset :get] [{:keys [db preloads]}]
  {:presets (model/presets db {:include-hidden true})
   :tags (model/tags db)
   :preset (merge (:preset preloads)
                  (model/preset-stats db (get-in preloads [:preset :preset/id])))})


(defn- remap-preset-form-data [form-data]
  (let [base-attrs #:preset{:label (get form-data :label)
                            :type (get form-data :type)
                            :is_hidden (get form-data :is_hidden false)
                            :reference (get form-data :reference)}
        tag-ids (:tag-ids form-data)]
    (assoc base-attrs
           :tag-ids tag-ids
           :tags (->> (utilities/<-json (get form-data :tags "[]"))
                      (mapv #(utilities/map->ns-map "tag" %))))))


(defmethod data-handler [:preset-form-template :post] [{:keys [db parameters route-substitutor]}]
  (let [preset-params (remap-preset-form-data (:form parameters))
        new-record (model/add-preset-with-tags db preset-params)]
    (with-meta new-record
      {:status 201
       :headers {"hx-redirect" (route-substitutor ^:route [:page-preset {:preset-id (:preset/id new-record)}])}})))


(defmethod data-handler [:preset-form :put] [{:keys [db parameters route-substitutor preloads]}]
  (let [preset-id (get-in preloads [:preset :preset/id])]
    (model/update-preset db preset-id (remap-preset-form-data (:form parameters)))
    (with-meta {}
      {:status 204
       :headers {"hx-redirect" (route-substitutor ^:route [:page-preset {:preset-id preset-id}])}})))


(defmethod data-handler [:page-goals :get] [{:keys [db]}]
  {:goals (model/tagged-goals db)})


(defmethod data-handler [:page-goal :get] [{:keys [db preloads]}]
  {:goals (model/tagged-goals db)
   :goal (:goal preloads)})


(defmethod data-handler [:tag-form-template :get] [_]
  {:tag #:tag{:category "generic"
              :category_label "Generic"
              :value "Placeholder"
              :is_hidden false}})


(defmethod data-handler [:tag-form-template :post] [{:keys [db parameters]}]
  (with-meta {:tag (model/add-tag db (utilities/map->ns-map "tag" (:form parameters)))}
    {:status 201}))


(defmethod data-handler [:tag-inline-form :get] [{:keys [preloads parameters]}]
  {:tag (:tag preloads)
   :edit (boolean (get-in parameters [:query :edit]))})


(defmethod data-handler [:tag-inline-form :put] [{:keys [db preloads parameters]}]
  (let [tag-id (get-in preloads [:tag :tag/id])]
    (model/update-tag db tag-id (:form parameters))
    (model/tag db tag-id)))


(defn- extract-referer-query [req]
  (when-let [referer-str (get-in req [:headers "referer"])]
    (let [url (java.net.URL. referer-str)]
      (.getQuery url))))


(defmethod data-handler [:clocked-interval-form :delete] [{:keys [db preloads route-substitutor] :as req}]
  ;; TODO pass referer query params to the redirect url so the user ends up on the same page as they were before
  (model/delete-interval db (get-in preloads [:interval :clocked_interval/id]))
  (with-meta {}
    {:status 204
     :headers {"hx-redirect" (route-substitutor ^:route [:page-intervals])}}))


(defmethod data-handler [:clocked-interval-form :put] [{:keys [db preloads parameters]}]
  (let [interval (:interval preloads)
        params (:form parameters)
        timezone (t/zone (:timezone params))
        start-at (-> (t/date-time (:start_at params))
                     (t/in timezone)
                     t/instant)
        end-at (-> (t/date-time (:end_at params))
                   (t/in timezone)
                   t/instant)]
    (if (t/<= end-at start-at)
      (throw (ex-info "invalid interval bounds"
                      {:status 400
                       :title "Bad Request"
                       :detail "`end_at` cannot precede `start_at`"}))
      (let [new-interval (model/update-interval
                          db
                          {:clocked_interval/id (:clocked_interval/id interval)
                           :clocked_interval/active_during {:tick/beginning start-at
                                                            :tick/end end-at}
                           :preset/id (:preset_id params)})]
        (with-meta {:interval new-interval
                    :presets (model/presets db)}
          {:headers {"hx-trigger" "reload-intervals-navigation-panel"}})))))


(defmethod data-handler [:intervals-navigation-panel :get] [{:keys [db parameters]}]
  (let [preset-id (get-in parameters [:query :preset_id])
        interval-id (get-in parameters [:query :interval_id])]
    {:intervals (model/clocked-intervals db {:preset/id preset-id})
     :presets (model/presets db)
     :selected-preset-id preset-id
     :selected-interval-id interval-id}))


(defmethod data-handler [:page-goodreads-preset-candidates :get] [{:keys [db handler-name]}]
  {:presets (model/presets db {:include-hidden true})
   :current-handler-id handler-name})


(defmethod data-handler [:goodreads-preset-candidates :get] [{:keys [db]}]
  {:presets (model/presets db {:include-hidden true})
   :books (->> (goodreads/get-reviews {:shelf "currently-reading"})
               (mapv goodreads/remap-review))})


(defmethod data-handler [:page-openlibrary-preset-candidates :get] [{:keys [db handler-name]}]
  {:presets (model/presets db {:include-hidden true})
   :current-handler-id handler-name})


(defmethod data-handler [:openlibrary-search :post] [{:keys [parameters]}]
  (let [response (openlibrary/get-search (get-in parameters [:form :query]))]
    {:results (:docs response)}))


(defmethod data-handler :default [{:keys [handler-name]}]
  (let [allowed-methods
        (->> (methods data-handler)
             keys
             (filter #(not= :default %))
             (filter #(= handler-name (first %)))
             (map second)
             (map (comp str/upper-case name))
             sort)]
    (if (seq allowed-methods)
      (throw (ex-info "no matching method defined in data handler"
                      {:status 405
                       :title "Method Not Allowed"
                       :allowed-methods allowed-methods}))
      (throw (ex-info "no matching resource defined in data handler"
                      {:status 404
                       :title "Not Found"})))))
