(ns activity-tracker.model
  (:require
   [clojure.set :as set]
   [next.jdbc :as jdbc]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   [activity-tracker.db.methods :as db.methods]
   ;; [activity-tracker.db.preset :as db.preset]
   ;; [activity-tracker.db.tag :as db.tag]
   ;; [activity-tracker.db.goal :as db.goal]
   ;; [activity-tracker.db.interval :as db.interval]
   [activity-tracker.utilities :refer [map->ns-map ns-map->map]]))


(defn presets-with-tags [db]
  (db.methods/get-visible-presets-with-tags db))

(defn preset-with-tags [db preset-id]
  (db.methods/get-preset-with-tags db preset-id))

(defn clock-in-preset [db preset-id]
  ;; TODO add exclude constraint so there is only one active interval at a time
  (jdbc/with-transaction [tx db]
    (let [currently-active (db.methods/get-active-interval db preset-id)]
      (when-not currently-active
        (db.methods/insert-active-interval db preset-id)))))

(defn clock-out-preset [db preset-id]
  (db.methods/deactivate-active-intervals db preset-id))

(defn preset-tag-groups [tags {:preset/keys [layout]}]
  ;; TODO unkludge this function, especially the order preservation logic
  (let [layout-categories (into #{} layout)
        by-layout (->> tags
                       (sort-by :tag/category_label)
                       (group-by (comp boolean layout-categories :tag/category)))
        layout-order (zipmap layout (range))]
    {:layout (sort-by (comp layout-order :tag/category) (or (by-layout true) []))
     :non-layout (or (by-layout false) [])}))

(defn related-tags-on-other-presets [db preset-id]
  (->> (db.methods/get-related-visible-tags-on-other-presets db preset-id)
       (mapv (fn [record]
               (-> record
                   (assoc :tag/is_active (:is_active record))
                   (dissoc :is_active))))))

(defn clocked-intervals
  ([db]
   (db.methods/get-intervals-of-non-deleted-presets db))
  ([db {preset-id :preset/id}]
   (->> (if preset-id
          (db.methods/get-intervals-by-preset db preset-id)
          (db.methods/get-intervals-of-non-deleted-presets db)))))

(defn presets
  ([db] (presets db {}))
  ([db query-options]
   (db.methods/get-presets db query-options)))

(defn tagged-goals [db]
  (db.methods/get-tagged-goals db))

(defn tagged-and-clocked-goal [db goal-id]
  (db.methods/get-tagged-and-clocked-goal db goal-id))

(defn tags-of-non-hidden-presets [db]
  (db.methods/get-tags-of-non-hidden-presets db))

(defn clocked-intervals-with-goals [db]
  (db.methods/get-intervals-with-goals db))

(defn interval-with-preset-data [db interval-id]
  (db.methods/get-interval-with-preset-label db interval-id))

(defn tags [db]
  (db.methods/get-tags db))

(defn tag [db tag-id]
  (db.methods/get-tag db tag-id))

(defn update-tag [db tag-id tag-params]
  (db.methods/update-tag db tag-id tag-params))

(defn add-tag [db tag-params]
  (db.methods/insert-tag db tag-params))

(defn maybe-add-tag [db tag-params]
  (let [params (ns-map->map tag-params)]
    (if-let [existing-tag (db.methods/get-tag-by-attrs db params)]
      existing-tag
      (db.methods/insert-tag db (assoc params :is_hidden false)))))

(defn add-preset-with-tags [db params]
  (jdbc/with-transaction [tx db]
    (let [new-record (db.methods/insert-preset db (ns-map->map params))
          new-tags (->> (:tags params)
                        (map #(maybe-add-tag db %)))]
      (doseq [tag-id (concat (:tag-ids params)
                             (map :tag/id new-tags))]
        (db.methods/insert-preset-tag db {:preset-id (:preset/id new-record)
                                          :tag-id tag-id}))
      new-record)))

(defn update-preset [db preset-id params]
  (jdbc/with-transaction [tx db]
    (let [existing-record (preset-with-tags db preset-id)
          existing-tag-ids (->> (:preset/tags existing-record)
                                (map :tag/id)
                                (into #{}))
          tags-add (set/difference (:tag-ids params) existing-tag-ids)
          tags-remove (set/difference existing-tag-ids (:tag-ids params))]
      (db.methods/update-preset db preset-id (ns-map->map params))
      (doseq [tag-id tags-add]
        (db.methods/insert-preset-tag db {:preset-id preset-id, :tag-id tag-id}))
      (doseq [tag-id tags-remove]
        (db.methods/delete-preset-tag db {:preset-id preset-id, :tag-id tag-id}))
      (preset-with-tags db preset-id))))

(defn delete-interval [db interval-id]
  (db.methods/soft-delete-interval db interval-id))

(defn update-interval [db params]
  (db.methods/update-interval
   db
   (:clocked_interval/id params)
   {:interval-id (:clocked_interval/id params)
    :active_during (format "[%s,%s)"
                           (get-in params [:clocked_interval/active_during :tick/beginning])
                           (get-in params [:clocked_interval/active_during :tick/end]))
    :preset_id (:preset/id params)})

  ;; TODO maybe this can be simpler thatn `interval-with-preset-data`
  (interval-with-preset-data db (:clocked_interval/id params)))

(defn preset-stats [db preset-id]
  (let [intervals (db.methods/get-intervals-by-preset db preset-id)
        sum (->> intervals
                 (map (fn [{:clocked_interval/keys [active_during]}]
                        (t/duration active_during)))
                 (reduce t/+))]
    {:preset/id preset-id
     :total-duration sum}))
