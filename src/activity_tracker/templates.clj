(ns activity-tracker.templates
  (:require
   [activity-tracker.components :as components]
   [clojure.pprint :refer [pprint]]
   [hiccup.core :as hiccup]))

(defmulti hiccup-formatter
  (fn [{{:keys [handler-name request-method]} :meta error :error :as data}]
    (if error
      [:error 404]
      [handler-name request-method])))

(defmethod hiccup-formatter [:page-index :get] [{:keys [content]}]
  [:div.container
   (->> (:presets content)
        (sort-by #(-> % :preset/latest_interval :clocked_interval/active_during :tick/end)
                 (fn [a b]
                   (cond
                     (nil? a) -1
                     (nil? b) 1
                     :else (compare b a))))
        (map (fn [preset]
               [:div
                {:hx-get ^:route [:preset-card {:preset-id (:preset/id preset)}]
                 :hx-swap "innerHTML"
                 :hx-trigger "intersect once"}
                (components/preset-card preset)])))])

(defmethod hiccup-formatter [:preset-card :get] [{:keys [content]}]
  (components/preset-card (:preset content)))

(defmethod hiccup-formatter [:page-timeline-overview :get] [{:keys [content]}]
  (let [{:keys [goals tags params]} content]
    (if (seq goals)
      [:div.container.box
       [:div.title "Clocked Time"]
       [:div {:style {:width "100%"}}
        (components/clocked-time-chart
         {:intervals {:url (:intervals-data-uri content)}
          :dates {:url (:intervals-dates-uri content)}}
         {:goals goals
          :tags tags
          :params params})]]
      [:div.container.box
       "No goals defined"])))

(defmethod hiccup-formatter [:page-login :get] [{:keys [content]}]
  [:div.container
   [:div.box
    [:form
     {:action "/api/login"
      :method "POST"}
     [:input
      {:type "hidden"
       :id "location"
       :name "location"
       :value (:return-location content)}]
     [:input.input
      {:id "username"
       :name "username"
       :placeholder "Username"}]
     [:input.input
      {:type "password"
       :id "password"
       :name "password"
       :placeholder "Password"}]
     [:input.input.button.is-primary
      {:type "submit"
       :value "Submit"}]]]])

(defmethod hiccup-formatter [:preset-card-actions :post] [{:keys [content]}]
  (concat
   (list (components/preset-card (:preset content)))
   (for [tag (:related-tags content)]
     (components/oob-swap
      (if (:is_layout tag)
        (components/preset-tag tag)
        (components/tag-card tag {:mixin :.is-medium}))))))

(defmethod hiccup-formatter [:page-intervals :get] [{:keys [content]}]
  [:div.container
   (components/intervals-navigation-panel
    (:clocked-intervals content)
    (:presets content)
    {:selected-preset-id (:selected-preset-id content)})])

(defmethod hiccup-formatter [:page-interval-detail :get] [{:keys [content]}]
  ;; TODO on tight layout, show the form section above the interval list, not next to it
  (let [{:keys [clocked-intervals presets interval selected-preset-id]} content]
    [:div.container
     [:div.columns.is-gapless.is-flex-wrap-wrap-reverse.is-flex
      [:div.column
       (components/intervals-navigation-panel
        clocked-intervals
        presets
        {:selected-preset-id selected-preset-id
         :selected-interval-id (:clocked_interval/id interval)})]
      [:div.column.is-narrow
       (components/interval-detail interval presets)]]]))

(defmethod hiccup-formatter [:page-presets :get] [{:keys [content parameters]}]
  [:div.container.box
   [:div.columns
    [:div.column
     (components/preset-navigation-panel
      (:presets content)
      {:current-handler-id (:current-handler-id content)
       :search (get-in parameters [:query :search])})]
    [:div.column
     (components/preset-action-tabs {:current-handler-id (:current-handler-id content)})
     [:div.box
      [:h3.title "Create"]
      (components/preset-form nil content)]]]])

(defmethod hiccup-formatter [:page-preset :get] [{:keys [content parameters]}]
  (let [{:keys [presets tags preset]} content]
    [:div.container.box
     [:div.columns
      [:div.column
       (components/preset-navigation-panel
        presets
        {:selected-id (:preset/id preset)
         :search (get-in parameters [:query :search])})]
      [:div.column
       (components/preset-action-tabs {:current-handler-id (:current-handler-id content)})
       [:div.box
        [:h3.title "Stats"]
        (components/preset-stats preset)]
       [:div.box
        [:h3.title "Edit"]
        (components/preset-form preset {:tags tags})]]]]))

(defmethod hiccup-formatter [:page-goals :get] [{:keys [content]}]
  [:div.container
   (->> (:goals content)
        (map (fn [goal]
               [:div
                {:hx-get ^:route [:page-goal {:goal-id (:goal/id goal)}]
                 :hx-select ".goal-details"
                 :hx-swap "innerHTML"
                 :hx-trigger "intersect once"}])))])

(defmethod hiccup-formatter [:page-goal :get] [{:keys [content]}]
  [:div.container
   [:div.columns.is-gapless
    [:div.column
     (components/goals-navigation-panel
      (:goals content)
      {:selected-id (get-in content [:goal :goal/id])})]
    [:div.column
     (components/goal-details (:goal content))]]])

(defmethod hiccup-formatter [:tag-form-template :get] [{:keys [content]}]
  (components/tag-editor-row-edit (:tag content)))

(defmethod hiccup-formatter [:tag-form-template :post] [{:keys [content]}]
  (components/tag-editor-row-view (:tag content)))

(defmethod hiccup-formatter [:tag-inline-form :get] [{:keys [content]}]
  (if (:edit content)
    (components/tag-editor-row-edit (:tag content))
    (components/tag-editor-row-view (:tag content))))

(defmethod hiccup-formatter [:tag-inline-form :put] [{:keys [content]}]
  (list
   (components/tag-editor-row-view content)
   (components/oob-swap (components/tag-selector-tag-label content))))

(defmethod hiccup-formatter [:clocked-interval-form :put] [{:keys [content]}]
  (components/interval-detail (:interval content) (:presets content)))

(defmethod hiccup-formatter [:intervals-navigation-panel :get] [{:keys [content]}]
  (components/intervals-navigation-panel
   (:intervals content)
   (:presets content)
   {:selected-preset-id (:selected-preset-id content)
    :selected-interval-id (:selected-interval-id content)}))

(defmethod hiccup-formatter [:page-goodreads-preset-candidates :get] [{:keys [content]}]
  [:div.container.box
   [:div.columns
    [:div.column
     (components/preset-navigation-panel
      (:presets content)
      {:current-handler-id (:current-handler-id content)})]
    [:div.column
     (components/preset-action-tabs {:current-handler-id (:current-handler-id content)})
     [:div.panel
      [:p.panel-heading
       "Goodreads"]
      [:div.panel-block
       [:div.control
        {:hx-get ^:route [:goodreads-preset-candidates]
         :hx-target "closest .panel-block"
         :hx-trigger "load"
         :hx-swap "outerHTML"}
        [:label.label "Loading data"]
        [:progress.progress.is-small.is-dark]]]]]]])

(defmethod hiccup-formatter [:goodreads-preset-candidates :get] [{:keys [content]}]
  (components/goodreads-book-candidates
   (:books content)
   {:existing-presets (->> (:presets content)
                           (map :preset/reference)
                           (filter identity)
                           (into #{}))}))

(defmethod hiccup-formatter [:page-openlibrary-preset-candidates :get] [{:keys [content]}]
  [:div.container.box
   [:div.columns
    [:div.column
     (components/preset-navigation-panel
      (:presets content))]
    [:div.column
     (components/preset-action-tabs {:current-handler-id (:current-handler-id content)})
     (components/openlibrary-search-panel)]]])

(defmethod hiccup-formatter [:openlibrary-search :post] [{:keys [content]}]
  (components/openlibrary-book-candidates (:results content)))

(defmethod hiccup-formatter [:error 404] [data]
  [:h3 (:title data)])

(defmethod hiccup-formatter :default [data]
  [:div (with-out-str (pprint data))])
