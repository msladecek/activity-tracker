(ns activity-tracker.utilities
  (:require
   [jsonista.core :as json]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]))


(defn extend-kw [base-kw & extensions]
  (let [base-namespace (namespace base-kw)
        new-name (apply str (conj (map name extensions)
                                  (name base-kw)))]
    (keyword base-namespace new-name)))


(defn end-of-day [instant]
  (-> instant
      (t/>> (t/new-duration 1 :days))
      (t/truncate :days)))


(defn start-of-week
  ([datetime] (start-of-week datetime {}))
  ([datetime {:keys [timezone] :or {timezone "Europe/Prague"}}]
   (let [days-since (-> datetime
                        (t/in timezone)
                        t/date-time
                        t/day-of-week
                        (.getValue))]
     (-> (t/instant datetime)
         (t/<< (t/new-duration (dec days-since) :days))
         (t/in timezone)
         (.truncatedTo java.time.temporal.ChronoUnit/DAYS)))))


(defn start-of-month
  ([datetime] (start-of-month datetime {}))
  ([datetime {:keys [timezone] :or {timezone "Europe/Prague"}}]
   (-> (t/instant datetime)
       (t/in timezone)
       (.truncatedTo java.time.temporal.ChronoUnit/DAYS)
       (.withDayOfMonth 1))))


(defn start-of-year
  ([datetime] (start-of-year datetime {}))
  ([datetime {:keys [timezone] :or {timezone "Europe/Prague"}}]
   (-> (t/instant datetime)
       (t/in timezone)
       (.truncatedTo java.time.temporal.ChronoUnit/DAYS)
       (.withDayOfYear 1))))


(defn midnights-between [start end]
  (let [start (end-of-day start)]
    (when (t/< start end)
      (lazy-seq
       (cons start (midnights-between start end))))))


(defn split-by-day [record]
  (let [start (:tick/beginning record)
        end (:tick/end record)
        sections (partition 2 1 (concat [start] (midnights-between start end) [end]))]
    (map
     (fn [[start-at end-at]]
       (assoc record :tick/beginning start-at :tick/end end-at))
     sections)))


(defn map->ns-map [namespace- map-]
  (->> map-
       (map (fn [[k v]]
              [(if (and (keyword? k)
                        (not (qualified-keyword? k)))
                 (keyword namespace- (name k))
                 k)
               v]))
       (into {})))


(defn ns-map->map [map-]
  (reduce-kv (fn [m k v]
               (assoc m (if (keyword? k) (keyword (name k)) k) v))
             {}
             map-))


(def mapper (json/object-mapper {:decode-key-fn keyword}))
(def ->json json/write-value-as-string)
(def <-json #(json/read-value % mapper))


(defn concat-lines [lines]
  (->> lines
       (interpose \newline)
       (apply str)))
